;;; simplify-mode.el --- Utilities for Simplify developers.  -*- lexical-binding: t; -*-

;; Copyright (C) 2017  Sébastien Le Callonnec

;; Author: Sébastien Le Callonnec <sebastien@weblogism.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This mode provides utilities for Emacs users working on Simplify.

;;; Code:

(require 's)
(require 'elnode)


(defcustom simplify-httpie-location
  "/usr/local/bin/http"
  "Location of the HTTPie binary."
  :type 'string
  :group 'simplify)

(defcustom simplify-http-proxy
  nil
  "HTTP proxy for debugging purposes."
  :type 'string
  :group 'simplify)

(defcustom simplify-login-url
  "http://localhost/payments-web/j_spring_security_check"
  "URL used for logging in."
  :type 'string
  :group 'simplify)

(defcustom simplify-redirect-url
  "http://localhost/payments-web/app#/dashboard"
  "URL to redirect to once login is complete."
  :type 'string
  :group 'simplify)

(defcustom simplify-fuelable-location
  "/Users/sebastienlecallonnec/dev/fuelable"
  "Location of the fuelable project"
  :type 'string
  :group 'simplify)

(defcustom simplify-jira-url
  "https://labs.mastercard.com/jira/"
  "Jira URL"
  :type 'string
  :group 'simplify)

(defcustom simplify-build-config-files
  '("payments/grails-app/conf/BuildConfig.groovy"
    "payments-web/grails-app/conf/BuildConfig.groovy"
    "simplifySupport/grails-app/conf/BuildConfig.groovy")
  "BuildConfig file to update."
  :type 'list
  :group 'simplify)

(defcustom simplify-common-deps
  '(("commons" . "com.simplify:payments-common")
    ("common" . "com.simplify:payments-common")
    ("cobrand" . "com.simplify:cobranding-common"))
  "List of dependencies to update."
  :type '(alist :key-type (string :tag "Package name")
                :value-type (string :tag "Fully-qualified name"))
  :options '("commons" "common" "cobrand")
  :group 'simplify)


(defun simplify--post-login (login password)
  (let ((url simplify-login-url)
        (httpie-result nil))
    (let ((httpie-command (concat simplify-httpie-location
                                  " --form POST " url
                                  " j_username='" login
                                  "' j_password='" password
                                  "' --ignore-stdin --headers ")))
      (when (and (boundp 'simplify-http-proxy) simplify-http-proxy)
        (setq httpie-command (concat httpie-command " --proxy http:" simplify-http-proxy)))
      (setq httpie-result
            (shell-command-to-string httpie-command))
      (nth 1 (s-match "Set-Cookie: JSESSIONID=\\([A-Z0-9]+\\);[^\n]*\n" httpie-result)))))


(defun simplify-log-into-simplify (login password)
  "Log into Simplify using LOGIN and PASSWORD as credentials."
  (interactive)
  (let ((query_string (concat "?username=" (url-hexify-string login) "&password=" (url-hexify-string password))))
    (browse-url (concat "http://localhost:5444" query_string))))


(defun simplify--elnode-login-handler (httpcon)
  "Handler that POSTs credentials to Simplify login controller."
  (let* ((username (elnode-http-param httpcon "username"))
         (password (elnode-http-param httpcon "password")))
    (let ((jsessionid nil))
      (setq jsessionid (simplify--post-login username password))
      (elnode-http-header-set httpcon "Set-Cookie" (format "JSESSIONID=%s; Path=/payments-web/; HttpOnly " jsessionid))
      (elnode-send-redirect httpcon simplify-redirect-url))))


(defun simplify--get-credentials-from-region ()
  (let ((credentials-text (s-trim (s-chomp (buffer-substring-no-properties (mark) (point))))))
    (mapcar #'s-trim (split-string credentials-text "\n"))))


;;;###autoload
(defun simplify-login-with-credentials ()
  "Log into Simplify using credentials in region."
  (interactive)
  (let ((credentials (simplify--get-credentials-from-region)))
    (simplify-log-into-simplify (first credentials) (second credentials))))

(defun simplify-start-login-server ()
  (elnode-start 'simplify--elnode-login-handler :port 5444 :host "localhost"))

(defun simplify-stop-login-server ()
  (elnode-stop 5444))

(defun simplify--init-mode ()
  (message "initializing Simplify mode...")
  (simplify-start-login-server))

(add-hook 'kill-emacs-hook #'simplify-stop-login-server)


;;;###autoload
(defun simplify-create-dbscript (branch database)
  "Create a Db script following Simplify conventions.
The Db script applies for branch BRANCH, and must be applied to db DATABASE."
  (interactive "sBranch: \nsDatabase: ")
  (let* ((script-name nil)
         (db-name nil))
    (cond
     ((or (equal database "shared") (equal database "sc_shared"))
      (setq script-name (concat branch ".pre.shared.sql"))
      (setq db-name "sc_shared"))
     ((or (equal database "live") (equal database "sc_live"))
      (setq script-name (concat branch ".pre.sql"))
      (setq db-name "sc_live")))
    (if db-name
        (progn
          (find-file (concat simplify-fuelable-location
                             "/payments/database/pending-release/pre-deployment/" script-name))
          (insert (concat "-- " db-name "\n\n\n--Backout \n\n"))
          (goto-char (+ (length (concat "-- " db-name)) 2)))
      (error "Unknown database: %s" db-name))))


(defun simplify-diff-logtime ()
  "Prints the time difference between two successive log lines in the region."
  (interactive)
  (let ((start-time nil)
        (end-time nil))
    (save-excursion
      (goto-char (region-beginning))
      (when
          (re-search-forward "\\([0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\} [0-9]\\{2\\}:[0-9]\\{2\\}:[0-9]\\{2\\},[0-9]\\{3\\}\\)") nil t)
      (setq start-time (date-to-time (match-string-no-properties 1)))
      (forward-line 1)
      (when
          (re-search-forward "\\([0-9]\\{4\\}-[0-9]\\{2\\}-[0-9]\\{2\\} [0-9]\\{2\\}:[0-9]\\{2\\}:[0-9]\\{2\\},[0-9]\\{3\\}\\)" nil t)
        (setq end-time (date-to-time (match-string-no-properties 1)))))
    (message "Time diff: %s s" (time-subtract end-time start-time))))



;;;###autoload
(defun simplify-replace-with-message (start end code)
  "Replace the name of the site with a localized message property."
  (interactive "r\nsCode: ")
  (let* ((current-text (buffer-substring start end))
         (fixed-text (replace-regexp-in-string "Simplify\\( \\(Commerce\\)\\)?" "{0}" current-text)))
    (message "New text: %s" fixed-text)
    (kill-new code)
    (kill-new fixed-text)
    (delete-region start end)
    (insert (concat "<g:message code=\"" code  "\" args=\"${[siteName]}\" />"))))


;;;###autoload
(defun simplify-open-jira-ticket (start end)
  (interactive "r")
  (let ((ticket (buffer-substring start end)))
    (browse-url (format "%sbrowse/%s" simplify-jira-url ticket))))



;;;###autoload
(defun simplify-update-package-version (package new-version)
  "Update PACKAGE to version NEW-VERSION in some Grails dependency files."
  (interactive "sPackage: \nsVersion: ")
  (let ((package-name (assoc-default package simplify-common-deps)))
    (cl-flet ((replace-version-func
               (file-to-process)
               (with-temp-file (expand-file-name file-to-process)
                 (if (file-exists-p file-to-process)
                     (insert-file-contents file-to-process))
                 (set-buffer-file-coding-system 'utf-8)
                 (goto-char 1)
                 (while (re-search-forward (format "%s:\\([^'\"]+\\)" (regexp-quote package-name)) nil t)
                   (replace-match new-version nil nil nil 1)))))
      (mapc #'replace-version-func
            (mapcar (lambda (f) (expand-file-name f simplify-fuelable-location)) simplify-build-config-files)))))


(defvar simplify-mode-keymap
  (let ((keymap (make-sparse-keymap)))
    (define-key keymap (kbd "C-c s l") #'simplify-login-with-credentials)
    (define-key keymap (kbd "C-c s d") #'simplify-create-dbscript)
    (define-key keymap (kbd "C-c s j") #'simplify-open-jira-ticket)
    (define-key keymap (kbd "C-c s u") #'simplify-update-package-version)
    (define-key keymap (kbd "C-c s m") #'maximize-frame)
    (define-key keymap (kbd "C-c s -") #'simplify-diff-logtime)
    keymap)
  "Keymap for Simplify mode.")


;;; Minor mode
;;;###autoload
(define-minor-mode simplify-mode
  "Simplify project utilities."
  :group 'simplify
  :lighter " Smplfy"
  :keymap simplify-mode-keymap
  (if simplify-mode
      (progn
        (add-hook 'after-init-hook #'simplify--init-mode))
    (progn
      (remove-hook 'after-init-hook #'simplify--init-mode))))

;;;###autoload
(define-global-minor-mode simplify-global-mode
  simplify-mode
  (lambda () (simplify-mode 1))
  :group 'simplify)


(provide 'simplify-mode)
;;; simplify-mode.el ends here
