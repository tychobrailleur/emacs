
;;;; https://gist.github.com/tychobrailleur/b8c10ba215ed40afe258a8320671b584

(require 'seq)

(defgroup org-journal-list nil
  "Mode for listing Org files."
  :tag "Org Journal List"
  :group 'org)

(defcustom org-journal-list-default-dir
  "~/org/"
  "Default location for org files"
  :type 'directory
  :group 'org-journal-list)

(defcustom org-journal-list-display-alist
  '((side . left)
    (window-width . 40)
    (slot . -1))
  "Alist used to display notes buffer.
See `display-buffer-in-side-window' for example options."
  :type 'alist
  :group 'org-journal-list)

(defcustom org-journal-list-preview-size
  5
  "Number of lines visible in the preview of every org file."
  :type 'integer
  :group 'org-journal-list)


(defun org-journal-list--read-journal (path)
  (with-temp-buffer
    (insert-file-contents (concat org-journal-list-default-dir path))
    (split-string (buffer-string) "\n" t)))

(defun org-journal-list--read-first-n-lines (list n)
  (cond ((>= (length list) (+ n 1)) (seq-subseq list 1 n))
        ((>= (length list) 1) (nthcdr 1 list))
        (t list)))

(defun org-journal-list--read-journal-heads (path)
  (mapconcat (function (lambda (line) (format "  %s" line)))
             (org-journal-list--read-first-n-lines (org-journal-list--read-journal path)
                                                   org-journal-list-preview-size) "\n"))

(defun org-journal-list ()
  (interactive)
  (org-journal-list--init-mode))

(defun org-journal-list--init-mode ()
  (let ((journal-list-buffer (get-buffer-create (generate-new-buffer-name "journal-list.org")))
        (journal-file-list (directory-files org-journal-list-default-dir nil "\\.org$")))
    (with-current-buffer journal-list-buffer
      (org-mode)
      (setq journal-file-list (mapcar (lambda (item)
                                        (format "* [[file:%s/%s][%s]]\n\n  #+BEGIN_SRC\n%s\n  #+END_SRC\n"
                                                item
                                                org-journal-list-default-dir
                                                item
                                                (org-journal-list--read-journal-heads item)))
                                      journal-file-list))
      (insert (mapconcat #'identity journal-file-list "\n"))
      (org-journal-list-mode)
      (read-only-mode)
      (goto-char 1))
    (display-buffer-in-side-window journal-list-buffer org-journal-list-display-alist)))

(defvar org-journal-list-keymap
  (let ((keymap (make-sparse-keymap)))
    (define-key keymap (kbd "q") #'org-journal-list-mode)
    keymap)
  "Keymap for Org Journal List mode.")

(define-minor-mode org-journal-list-mode
  "Org journal list mode."
  :group 'org-journal-list
  :lighter (str " " (all-the-icons-icon-for-file "journal-list.org"))
  :keymap org-journal-list-keymap)


(provide 'org-journal-list)
;;; org-journal-list.el ends here
