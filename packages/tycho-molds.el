(require 'moldable-emacs)

(defun helm-mold--run-mold (mold)
  (--each me-mold-before-mold-runs-hook (funcall it mold))
  (me-mold-run-then mold)
  (run-hooks 'me-mold-after-hook))

(defun helm-molds ()
  (interactive)
  (run-hooks 'me-mold-before-hook)
  (let ((usable-molds (me-usable-molds)))
    (helm :sources (list
                    (helm-build-sync-source "Available Molds"
                                            :candidates
                                            (lambda () (cl-sort (mapcar (lambda (m) (plist-get m :key)) usable-molds)
                                                                'string-lessp :key 'downcase))
                                            :fuzzy-match t
                                            :action
                                            '(("Use Mold" . (lambda (m) (helm-mold--run-mold
                                                                         (me-find-mold m))))))))))

(defun helm-molds--plist-to-string (pl)
   (loop for property in pl collect `(,(car property) . ,(cdr property))))

(me-register-mold :key "AllOrgRoamTags"
                  :given (:fn 't)
                  :let ((tags (org-roam-db-query
                               [:select [(funcall count tag), tag]
                                        :from tags
                                        :group-by [tag]])))
                  :then (:fn (with-current-buffer buffername
                               (erase-buffer)
                               (org-mode)
                               (insert (me-plist-table-to-org-table (me-alist-to-plist (helm-molds--plist-to-string tags))))
                               (setq-local self tags))))

(provide 'tycho-molds)
