(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(TeX-PDF-mode t)
 '(TeX-auto-save t)
 '(TeX-engine 'default)
 '(TeX-parse-self t)
 '(TeX-view-program-selection
   '(((output-dvi style-pstricks) "dvips and gv") (output-dvi "xdvi") (output-pdf "Okular")
     (output-html "xdg-open")))
 '(abbrev-file-name "~/emacs/abbrev_defs")
 '(abbrev-suggest t)
 '(aeronaut-show-hidden-files t)
 '(ag-arguments '("--smart-case" "--stats" "--hidden"))
 '(ansi-color-faces-vector
   [default bold shadow italic underline bold bold-italic bold])
 '(apropos-sort-by-scores t)
 '(aquamacs-additional-fontsets nil t)
 '(aquamacs-customization-version-id 216 t)
 '(aquamacs-tool-bar-user-customization nil t)
 '(auth-source-save-behavior nil)
 '(backup-directory-alist `(("." . "~/.saves")))
 '(bibtex-completion-bibliography '("~/Dropbox/Documents/Bibliography/blockchain.bib"))
 '(bibtex-completion-library-path '("~/Dropbox/Documents/Bibliography"))
 '(bibtex-completion-notes-path "~/Dropbox/Documents/org/org-roam/")
 '(bmkp-last-as-first-bookmark-file "~/.emacs.d/bookmarks")
 '(browse-url-browser-function 'browse-url-default-browser)
 '(btc-ticker-amount 0.1905)
 '(calendar-date-style 'european)
 '(calendar-today-marker 'calendar-today)
 '(calendar-week-start-day 1)
 '(chess-default-display '(chess-images chess-ics1 chess-plain))
 '(chess-display-highlight-legal t)
 '(chess-plain-piece-chars
   '((75 . 75) (81 . 81) (82 . 82) (66 . 66) (78 . 78) (80 . 80) (107 . 107) (113 . 113) (114 . 114)
     (98 . 98) (110 . 110) (112 . 112)))
 '(cider-lein-parameters "repl :headless :host localhost")
 '(cider-repl-result-prefix ";; => ")
 '(cljr-warn-on-eval nil)
 '(column-number-mode t)
 '(command-log-mode-auto-show t)
 '(company-show-quick-access t nil nil "Customized with use-package company")
 '(company-tooltip-limit 20)
 '(confirm-nonexistent-file-or-buffer nil)
 '(connection-local-criteria-alist
   '(((:application vc-git) vc-git-connection-default-profile)
     ((:application tramp :protocol "kubernetes") tramp-kubernetes-connection-local-default-profile)
     ((:application eshell) eshell-connection-default-profile)
     ((:application tramp :protocol "flatpak")
      tramp-container-connection-local-default-flatpak-profile
      tramp-flatpak-connection-local-default-profile)
     ((:application tramp :machine "localhost") tramp-connection-local-darwin-ps-profile)
     ((:application tramp :machine "C02YM23WLVCJ-MCLABS") tramp-connection-local-darwin-ps-profile)
     ((:application tramp) tramp-connection-local-default-system-profile
      tramp-connection-local-default-shell-profile)))
 '(connection-local-profile-alist
   '((vc-git-connection-default-profile (vc-git--program-version))
     (eshell-connection-default-profile (eshell-path-env-list))
     (tramp-flatpak-connection-local-default-profile
      (tramp-remote-path "/app/bin" tramp-default-remote-path "/bin" "/usr/bin" "/sbin" "/usr/sbin"
                         "/usr/local/bin" "/usr/local/sbin" "/local/bin" "/local/freeware/bin"
                         "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin"
                         "/opt/bin" "/opt/sbin" "/opt/local/bin"))
     (tramp-kubernetes-connection-local-default-profile
      (tramp-config-check . tramp-kubernetes--current-context-data)
      (tramp-extra-expand-args 97 (tramp-kubernetes--container (car tramp-current-connection)) 104
                               (tramp-kubernetes--pod (car tramp-current-connection)) 120
                               (tramp-kubernetes--context-namespace (car tramp-current-connection))))
     (tramp-container-connection-local-default-flatpak-profile
      (tramp-remote-path "/app/bin" tramp-default-remote-path "/bin" "/usr/bin" "/sbin" "/usr/sbin"
                         "/usr/local/bin" "/usr/local/sbin" "/local/bin" "/local/freeware/bin"
                         "/local/gnu/bin" "/usr/freeware/bin" "/usr/pkg/bin" "/usr/contrib/bin"
                         "/opt/bin" "/opt/sbin" "/opt/local/bin"))
     (tramp-connection-local-darwin-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o"
                                        "pid,uid,user,gid,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "-o" "state=abcde" "-o"
                                        "ppid,pgid,sess,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etime,pcpu,pmem,args")
      (tramp-process-attributes-ps-format (pid . number) (euid . number) (user . string)
                                          (egid . number) (comm . 52) (state . 5) (ppid . number)
                                          (pgrp . number) (sess . number) (ttname . string)
                                          (tpgid . number) (minflt . number) (majflt . number)
                                          (time . tramp-ps-time) (pri . number) (nice . number)
                                          (vsize . number) (rss . number) (etime . tramp-ps-time)
                                          (pcpu . number) (pmem . number) (args)))
     (tramp-connection-local-busybox-ps-profile
      (tramp-process-attributes-ps-args "-o"
                                        "pid,user,group,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "-o" "stat=abcde" "-o" "ppid,pgid,tty,time,nice,etime,args")
      (tramp-process-attributes-ps-format (pid . number) (user . string) (group . string)
                                          (comm . 52) (state . 5) (ppid . number) (pgrp . number)
                                          (ttname . string) (time . tramp-ps-time) (nice . number)
                                          (etime . tramp-ps-time) (args)))
     (tramp-connection-local-bsd-ps-profile
      (tramp-process-attributes-ps-args "-acxww" "-o"
                                        "pid,euid,user,egid,egroup,comm=abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                        "-o"
                                        "state,ppid,pgid,sid,tty,tpgid,minflt,majflt,time,pri,nice,vsz,rss,etimes,pcpu,pmem,args")
      (tramp-process-attributes-ps-format (pid . number) (euid . number) (user . string)
                                          (egid . number) (group . string) (comm . 52)
                                          (state . string) (ppid . number) (pgrp . number)
                                          (sess . number) (ttname . string) (tpgid . number)
                                          (minflt . number) (majflt . number) (time . tramp-ps-time)
                                          (pri . number) (nice . number) (vsize . number)
                                          (rss . number) (etime . number) (pcpu . number)
                                          (pmem . number) (args)))
     (tramp-connection-local-default-shell-profile (shell-file-name . "/bin/sh")
                                                   (shell-command-switch . "-c"))
     (tramp-connection-local-default-system-profile (path-separator . ":")
                                                    (null-device . "/dev/null"))))
 '(copmany-echo-delay 0)
 '(custom-enabled-themes '(misterioso))
 '(custom-safe-themes
   '("f66ffeadda7b52d40c8d698967ae9e9836f54324445af95610d257fa5e3e1e21"
     "ec5f697561eaf87b1d3b087dd28e61a2fc9860e4c862ea8e6b0b77bd4967d0ba"
     "6b2636879127bf6124ce541b1b2824800afc49c6ccd65439d6eb987dbf200c36"
     "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223"
     "f0ea6118d1414b24c2e4babdc8e252707727e7b4ff2e791129f240a2b3093e32"
     "3ad55e40af9a652de541140ff50d043b7a8c8a3e73e2a649eb808ba077e75792" default))
 '(debug-on-error nil)
 '(deft-auto-save-interval 0.0)
 '(deft-extensions '("org" "txt"))
 '(deft-file-naming-rules '((noslash . "_") (nospace . "_") (case-fn . downcase)))
 '(deft-recursive t)
 '(deft-strip-summary-regexp ":PROPERTIES:\12\\(.+\12\\)+:END:\12")
 '(deft-strip-title-regexp
   ":PROPERTIES:\12\\(.+\12\\)+:END:\12\\(?:^%+\\|^#\\+[tT][iI][tT][lL][eE]: *\\|^[#* ]+\\|-\\*-[[:alpha:]]+-\\*-\\|^title:[\11 ]*\\|#+$\\)")
 '(deft-use-filename-as-title t)
 '(delete-old-versions t)
 '(desktop-save-mode nil)
 '(dired-dwim-target t)
 '(dired-recursive-copies 'always)
 '(diredp-hide-details-initially-flag nil)
 '(diredp-hide-details-propagate-flag t)
 '(display-line-numbers nil)
 '(display-time-24hr-format t)
 '(display-time-mode t)
 '(docker-run-as-root t)
 '(ecb-options-version "2.40")
 '(electric-pair-mode t)
 '(elfeed-feeds
   '("https://alvinalexander.com/rss.xml" "http://feeds.feedburner.com/glaforge?format=xml"
     "http://melix.github.io/blog/feed.xml" "http://oremacs.com/atom.xml"
     "http://gigasquidsoftware.com/atom.xml" "http://feeds.feedburner.com/emacsblog"
     "http://emacsredux.com/atom.xml" "https://auth0.com/blog/rss.xml"
     "https://www.coindesk.com/feed" "https://blog.ethereum.org/feed.xml"
     "https://www.microsoft.com/en-us/research/feed/" "https://media.mycelium.com/feed/"
     "https://www.siliconrepublic.com/category/innovation/feed" "https://ethresear.ch/latest.rss"
     "https://hackernoon.com/feed"))
 '(elfeed-goodies/entry-pane-position 'right)
 '(elfeed-goodies/entry-pane-size 0.4)
 '(elfeed-search-title-max-width 100)
 '(elfeed-use-curl t)
 '(epg-pinentry-mode 'ask)
 '(erc-nick "tychobrailleur")
 '(etelmgr-repository-url "http://mirrors.ircam.fr/pub/CTAN/systems/texlive/tlnet")
 '(etelmgr-texlive-dir "/usr/local/texlive/2023")
 '(fci-rule-color "#14151E")
 '(fill-column 100)
 '(flyspell-default-dictionary "british")
 '(garbage-collection-messages t)
 '(gc-cons-threshold 200000000)
 '(global-hl-line-mode t)
 '(global-linum-mode nil)
 '(gnus-summary-line-format "%U%R%z%I%(%[%4L: %-23,23f%]%) %d %s\12")
 '(guide-key-mode t)
 '(guide-key/idle-delay 0.5)
 '(haskell-mode-hook '(turn-on-eldoc-mode turn-on-haskell-simple-indent))
 '(helm-projectile-fuzzy-match nil)
 '(highlight-thing-what-thing 'word)
 '(history-length 1000)
 '(ido-confirm-unique-completion t)
 '(ido-create-new-buffer 'always)
 '(ido-enable-flex-matching t)
 '(ido-everywhere t)
 '(ido-mode 'both nil (ido))
 '(ignored-local-variable-values '((TeX-engine . context)))
 '(indent-tabs-mode nil)
 '(indicate-empty-lines nil)
 '(inhibit-startup-screen t)
 '(initial-scratch-message "")
 '(ispell-dictionary "english" t)
 '(jabber-history-enabled t)
 '(js-indent-level 2)
 '(kept-new-versions 6)
 '(keyfreq-file "~/.emacs.d/keyfreq")
 '(kill-whole-line t)
 '(large-file-warning-threshold 40000000)
 '(linum-format "%d ")
 '(lsp-java-trace-server "verbose")
 '(lsp-ui-imenu-auto-refresh t)
 '(magit-remote-add-set-remote.pushDefault "origin")
 '(magit-revert-item-confirm nil)
 '(mail-user-agent 'mu4e-user-agent)
 '(major-mode-remap-alist
   '((doctex-mode . docTeX-mode) (latex-mode . LaTeX-mode) (texinfo-mode . Texinfo-mode)
     (plain-tex-mode . plain-TeX-mode) (tex-mode . TeX-tex-mode)))
 '(make-backup-files t)
 '(max-lisp-eval-depth 10000)
 '(max-specpdl-size 13000 t)
 '(mouse-wheel-progressive-speed nil)
 '(mouse-wheel-scroll-amount '(5 ((shift) . 1) ((control))))
 '(mu4e-attachment-dir "~/Downloads")
 '(mu4e-bookmarks
   '((:name "Unread messages" :query
            "flag:unread AND NOT flag:trashed AND NOT maildir:\"/yahoo/Bulk Mail\" AND NOT  maildir:/weblogism/INBOX.Junk"
            :key 117)
     (:name "Today's messages" :query "date:today..now" :key 116)
     (:name "Last 7 days" :query "date:7d..now" :hide-unread t :key 119)
     (:name "Messages with images" :query "mime:image/*" :key 112)))
 '(mu4e-compose-context-policy 'pick-first)
 '(mu4e-compose-reply-to-address "sebastien@weblogism.com")
 '(mu4e-compose-signature-auto-include nil)
 '(mu4e-context-policy 'pick-first)
 '(mu4e-drafts-folder "/weblogism/INBOX.Draft")
 '(mu4e-get-mail-command "~/bin/fetch_my_mail.sh")
 '(mu4e-headers-date-format "%d/%m/%y")
 '(mu4e-headers-visible-lines 20)
 '(mu4e-html2text-command 'mu4e-shr2text)
 '(mu4e-maildirs-extension-after-insert-maildir-hook '(mu4e-maildirs-extension-insert-newline-when-unread))
 '(mu4e-maildirs-extension-hide-empty-maildirs t)
 '(mu4e-maildirs-extension-use-bookmarks t)
 '(mu4e-sent-folder "/weblogism/INBOX.Sent")
 '(mu4e-trash-folder "/weblogism/INBOX.Trash")
 '(mu4e-update-interval 300)
 '(mu4e-view-show-images t)
 '(neo-vc-integration '(char))
 '(neo-window-width 45)
 '(notmuch-search-oldest-first nil)
 '(ns-tool-bar-display-mode nil t)
 '(ns-tool-bar-size-mode nil t)
 '(org-adapt-indentation t)
 '(org-agenda-compact-blocks nil t)
 '(org-agenda-entry-text-leaders "    | ")
 '(org-agenda-files '("~/org/todo.org"))
 '(org-agenda-loop-over-headlines-in-active-region nil)
 '(org-agenda-start-with-entry-text-mode t)
 '(org-babel-load-languages '((emacs-lisp) (ruby . t) (shell . t) (sql . t)))
 '(org-default-notes-file "~/org/todo.org")
 '(org-ellipsis " ⤷")
 '(org-export-backends '(ascii html latex md odt confluence))
 '(org-export-coding-system 'utf-8)
 '(org-export-with-toc nil)
 '(org-highlight-latex-and-related '(latex))
 '(org-image-actual-width '(0))
 '(org-latex-classes
   '(("article" "\\documentclass[10pt]{article}" ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}") ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
      ("\\paragraph{%s}" . "\\paragraph*{%s}") ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
     ("report" "\\documentclass[10pt]{report}" ("\\part{%s}" . "\\part*{%s}")
      ("\\chapter{%s}" . "\\chapter*{%s}") ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}") ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
     ("book" "\\documentclass[10pt]{book}" ("\\part{%s}" . "\\part*{%s}")
      ("\\chapter{%s}" . "\\chapter*{%s}") ("\\section{%s}" . "\\section*{%s}")
      ("\\subsection{%s}" . "\\subsection*{%s}") ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
     ("memoir" "\\documentclass[10pt]{memoir}" ("\\chapter{%s}" . "\\chapter*{%s}")
      ("\\section{%s}" . "\\section*{%s}") ("\\subsection{%s}" . "\\subsection*{%s}")
      ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))
 '(org-log-done 'time)
 '(org-md-headline-style 'atx)
 '(org-pretty-entities nil)
 '(org-ref-default-bibliography '("~/Dropbox/Documents/Bibliography/blockchain.bib"))
 '(org-ref-pdf-directory '("~/Dropbox/Documents/Bibliography/"))
 '(org-remark-global-tracking-mode t)
 '(org-remark-use-org-id t)
 '(org-return-follows-link t)
 '(org-safe-remote-resources '("\\`https://fniessen\\.github\\.io\\(?:/\\|\\'\\)"))
 '(org-side-tree-persistent t)
 '(org-startup-with-inline-images t)
 '(package-hidden-regexps '("\\`groovy-mode"))
 '(package-native-compile t)
 '(package-quickstart nil)
 '(package-selected-packages
   '(ace-jump-mode ag all-the-icons auctex butler casual cdlatex citar-org-roam clj-refactor
                   company-erlang company-prescient crux csv-mode diff-hl dirvish docker
                   dockerfile-mode editorconfig edts elfeed-goodies elfeed-org elfeed-score
                   elisp-refs ellama elnode embark emmet-mode erlstack-mode exec-path-from-shell
                   fancy-dabbrev flycheck-clj-kondo flycheck-kotlin focus geiser-guile go-mode
                   golden-ratio helm-ag helm-apt helm-bibtex helm-bufler helm-org helm-org-rifle
                   helm-projectile hnreader ibuffer-vc idle-highlight-mode ido-vertical-mode
                   json-mode kotlin-mode kubernetes-helm logview lsp-java lsp-python-ms lsp-ui
                   magit-todos markdown-preview-mode moody neotree notmuch-maildir org-ac
                   org-contrib org-download org-drill org-journal org-modern org-ql org-ref
                   org-remark org-roam-ui org-side-tree org-transclusion ox-reveal pcsv pkg-info
                   plantuml-mode prodigy protobuf-mode racer rainbow-delimiters restclient slime
                   smex templatel terraform-mode toml-mode tree-sitter-langs typescript-mode
                   undo-tree visual-fill-column vterm vulpea x509-mode))
 '(paradox-automatically-star t)
 '(paradox-execute-asynchronously t)
 '(paradox-github-token t t)
 '(plantuml-server-url "http://localhost:8000")
 '(prettify-symbols-unprettify-at-point 'right-edge)
 '(projectile-switch-project-action 'magit-status)
 '(reb-re-syntax 'string)
 '(recentf-max-saved-items 500)
 '(recentf-mode t)
 '(reftex-default-bibliography '("~/Dropbox/Documents/Bibliography/blockchain.bib"))
 '(reftex-plug-into-AUCTeX t)
 '(require-final-newline t)
 '(rmh-elfeed-org-files '("~/org/elfeed.org"))
 '(safe-local-variable-values
   '((Syntax . ANSI-Common-Lisp) (Base . 10) (ispell-dictionary . "francais")
     (TeX-auto-regexp-list . LaTeX-auto-regexp-list) (TeX-auto-untabify) (TeX-auto-save)
     (TeX-parse-self) (TeX-brace-indent-level . 4) (TeX-engine . tex) (TeX-engine . XeTeX)
     (engine . xelatex) (etags-regen-ignores "test/manual/etags/")
     (etags-regen-regexp-alist
      (("c" "objc") "/[ \11]*DEFVAR_[A-Z_ \11(]+\"\\([^\"]+\\)\"/\\1/"
       "/[ \11]*DEFVAR_[A-Z_ \11(]+\"[^\"]+\",[ \11]\\([A-Za-z0-9_]+\\)/\\1/"))
     (system-time-locale . "C")
     (c-file-offsets (innamespace . 0) (inline-open . 0) (case-label . +))
     (c-file-offsets (innamespace . 0) (inline-open . 0)) (folded-file . t)
     (eval visual-fill-column-width 999999) (eval visual-fill-column-width nil)
     (TeX-engine . xelatex) (TeX-engine . latex)
     (eval setq org-latex-default-packages-alist
           (cons '("mathletters" "ucs" nil) org-latex-default-packages-alist))
     (org-latex-inputenc-alist ("utf8" . "utf8x")) (grails . 1) (grails-mode . 1)
     (bug-reference-bug-regexp . "<https?://\\(debbugs\\|bugs\\)\\.gnu\\.org/\\([0-9]+\\)>")
     (js2-basic-offset . 4) (allout-layout . t) (frozen-string-literal . true)
     (json-reformat:indent-width . 2) (css-indent-offset . 4) (web-mode-css-indent-offset 4)
     (web-mode-markup-indent-offset . 4) (js2-basic-offset 2) (css-indent-offset . 2)
     (web-mode-css-indent-offset 2) (web-mode-markup-indent-offset . 2) (c-hanging-comment-ender-p)
     (encoding . utf-8) (TeX-auto-save . t) '(reftex-plug-into-AUCTeX t) (TeX-parse-self . t)))
 '(savehist-mode t)
 '(scroll-bar-mode nil)
 '(scroll-conservatively 101)
 '(scroll-preserve-screen-position 1)
 '(scss-compile-at-save t)
 '(send-mail-function 'smtpmail-send-it)
 '(simplify-http-proxy "localhost:9000")
 '(simplify-login-url "http://localhost/payments-web/j_spring_security_check")
 '(simplify-redirect-url "http://localhost/payments-web/app")
 '(smtpmail-smtp-server "ssl0.ovh.net")
 '(smtpmail-smtp-service 25)
 '(sql-connection-alist
   '(("expert" (sql-product 'mysql) (sql-user "root") (sql-password "") (sql-server "localhost")
      (sql-database "example") (sql-port 3306))))
 '(tab-width 4)
 '(tool-bar-mode nil)
 '(track-eol 1)
 '(tramp-default-method "ssh")
 '(tramp-syntax 'default nil (tramp))
 '(treemacs-width 42)
 '(undo-tree-auto-save-history nil)
 '(user-mail-address "sebastien@weblogism.com")
 '(vc-annotate-background nil)
 '(vc-annotate-color-map
   '((20 . "#d54e53") (40 . "goldenrod") (60 . "#e7c547") (80 . "DarkOliveGreen3") (100 . "#70c0b1")
     (120 . "DeepSkyBlue1") (140 . "#c397d8") (160 . "#d54e53") (180 . "goldenrod")
     (200 . "#e7c547") (220 . "DarkOliveGreen3") (240 . "#70c0b1") (260 . "DeepSkyBlue1")
     (280 . "#c397d8") (300 . "#d54e53") (320 . "goldenrod") (340 . "#e7c547")
     (360 . "DarkOliveGreen3")))
 '(vc-annotate-very-old-color nil)
 '(vc-follow-symlinks t)
 '(version-control t)
 '(visible-bell t)
 '(visual-line-mode nil t)
 '(vm-preview-read-messages t)
 '(vterm-always-compile-module t)
 '(warning-suppress-log-types '((comp)))
 '(warning-suppress-types '((lsp-mode)))
 '(web-mode-enable-current-column-highlight t)
 '(web-mode-enable-current-element-highlight t)
 '(whitespace-display-mappings
   '((space-mark 32 [183] [46]) (newline-mark 10 [182 10]) (tab-mark 9 [9655 9] [92 9])))
 '(world-clock-list
   '(("Europe/Dublin" "Dublin") ("America/New York" "New York")
     ("America/San Francisco" "San Francisco") ("Australia/Melbourne" "Melbourne"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "DejaVu Sans Mono" :foundry "PfEd" :slant normal :weight regular :height 128 :width normal))))
 '(calendar-today ((t (:underline "orange red" :weight bold))))
 '(flyspell-incorrect ((t (:underline "Red1"))))
 '(helm-source-header ((t (:extend t :background "#22083397778B" :foreground "white" :weight normal :height 1.2 :family "Sans Serif"))))
 '(idle-highlight ((t (:background "#BFB8AD" :foreground "#2D3743"))))
 '(mode-line ((t (:background "#FF4500" :foreground "#eeeeec" :box (:color "#2C323C") :height 1.0))))
 '(org-remark-highlighter ((t (:background "dark cyan" :underline "purple3")))))
