(setq comp-speed 2)
(add-to-list 'load-path "~/emacs/packages")

;; Save location in files so that we re-open
;; at the same position.
(require 'saveplace)
(save-place-mode t)

(require 'package)
(add-to-list 'package-archives
             '("ELPA" . "http://tromey.com/elpa/"))
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("nongnu" . "https://elpa.nongnu.org/nongnu/") t)

(when (not package-archive-contents)
  (package-refresh-contents))

(mapc
 (lambda (package)
   (or (package-installed-p package)
       (package-install package)))
 '(ace-jump-mode ; dep for elfeed-goodies
   ace-window
   ag
   aio
   all-the-icons
   async
   auctex
   bufler
   burly
   butler
   casual ;; main entry point is casual-main-menu
   cdlatex
   cider ; clojure
   citar
   citar-org-roam
   clj-refactor
   company ; completion package
   company-erlang
   ;;   company-lsp
   company-prescient
   counsel
   crux
   csv-mode
   dash
   diff-hl
   dirvish
   docker
   dockerfile-mode
   editorconfig
   edts
   elfeed
   elfeed-goodies
   elfeed-org
   elfeed-score
   elisp-refs
   ellama
   elnode
   embark
   erlang
   erlstack-mode
   exec-path-from-shell ; makes sure the env variables in emacs are
                                        ; identical to bash
   fancy-dabbrev
   flycheck-clj-kondo
   flycheck-kotlin
   focus
   geiser ; scheme
   geiser-guile
   helm
   helm-ag
   helm-apt
   helm-bibtex
   helm-bufler
   helm-org
   helm-org-rifle
   helm-projectile
   hnreader
   ibuffer-vc
   idle-highlight-mode ;; highlights the word at point when idle
   ido-vertical-mode
;;   jenkinsfile-mode
   json-mode
   ;;   keyfreq
   kotlin-mode
   kubernetes-helm
   llm
   lsp-java
   lsp-mode
   lsp-ui
   magit
   magit-todos
   markdown-preview-mode
   moody
   multiple-cursors
   neotree
   notmuch-maildir
   org
   org-ac
   org-contrib
   org-download
   org-drill
   ;;   org-drill
   org-journal
;;   org-modern
;;   org-plus-contrib
   org-ql
   org-ref
   org-remark
   org-roam
   org-roam-ui
   org-side-tree
   org-super-agenda
   org-transclusion
   ox-reveal
   paredit
   pcre2el
   pcsv
   popwin ; dep for elfeed-goodies
   powerline ; dep for elfeed-goodies
   prescient
   prodigy
   protobuf-mode
   projectile
   pygn-mode
   lsp-python-ms
   racer
   rainbow-delimiters
   request ;; can be used to make http requests
   restclient
   rust-mode
   simple-httpd
   slime ; cl lisp
   smex
   string-inflection
   templatel
   terraform-mode
   toml-mode
   tree-sitter
   tree-sitter-langs
   typescript-mode
   undo-tree
   use-package
   visual-fill-column
   vterm
   vulpea
   yaml-mode
   yasnippet
   x509-mode
   ))

(setq debug-on-error t)
(require 'yasnippet)
(add-to-list 'yas-snippet-dirs "~/emacs/snippets")
(yas-global-mode 1)
(add-hook 'term-mode-hook (lambda () (setq yas-dont-activate-functions t)))

;; Nonsense to get stuff from emacswiki...
(unless (file-directory-p
         (expand-file-name (concat user-emacs-directory "emacswiki")))
  (require 'url-util)
  (make-directory (expand-file-name (concat user-emacs-directory "emacswiki")))
  (let ((urls-to-download '("https://www.emacswiki.org/emacs/download/dired%2b.el"
                            "https://www.emacswiki.org/emacs/download/frame-cmds.el"
                            "https://www.emacswiki.org/emacs/download/frame-fns.el"
                            "https://www.emacswiki.org/emacs/download/help-fns%2b.el")))
    (mapcar #'(lambda (u)
                (url-copy-file u
                               (concat (file-name-as-directory (expand-file-name (concat user-emacs-directory "emacswiki")))
                                       (url-unhex-string (car (last (split-string u "/")))))))
            urls-to-download)))

(add-to-list 'load-path (concat user-emacs-directory "emacswiki"))
(require 'frame-cmds)
(require 'help-fns+)

(setq ispell-program-name "hunspell")
(setq ispell-dictionary "en_GB")
;;(setq ispell-dictionary "british")

(projectile-mode)
;;;(setq projectile-enable-caching t)
(helm-projectile-on)

(use-package company
  :diminish company-mode
  :hook ((prog-mode ess-r-mode) . company-mode)
;;  :bind
  ;; (:map company-active-map
  ;;       ("TAB" . smarter-tab-to-complete))
  :custom
  (company-minimum-prefix-length 1)
  (company-tooltip-align-annotations t)
  (company-require-match 'never)
  ;; Don't use company in the following modes
  (company-global-modes '(not shell-mode eaf-mode))
  ;; Trigger completion immediately.
  (company-idle-delay 0.1)
  ;; Number the candidates (use M-1, M-2 etc to select completions).
  (company-show-numbers t)
  :config
  ;;(unless clangd-p (delete 'company-clang company-backends))
  (global-company-mode 1))

(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

(setq-default abbrev-mode t)
(global-undo-tree-mode t)

(require 'lsp-mode)
(add-hook 'rust-mode-hook #'lsp)
(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)

;; (require 'lsp-python-ms)
;; (add-hook 'python-mode-hook #'lsp)


;; (require 'lsp-java)
;; (add-hook 'java-mode-hook #'lsp)

(require 'flycheck-clj-kondo)

(add-hook 'erlang-mode-hook #'lsp)
(add-hook 'groovy-mode-hook #'lsp)
(add-hook 'typescript-mode-hook #'lsp)

(defun my-erlang-mode-hook ()
  (local-set-key "\C-c\C-c" 'erlang-compile)
  (if window-system
      (progn
        (setq font-lock-maximum-decoration t)
        (font-lock-mode 1)))
  (if (and window-system (fboundp 'imenu-add-to-menubar))
      (imenu-add-to-menubar "Imenu")))

;; (add-hook 'python-mode-hook
;;   (lambda () (setq indent-tabs-mode t)))


(use-package lsp-mode
  :commands (lsp lsp-deferred))

;; ;; ;; optional - provides fancier overlays
(use-package lsp-ui
  :commands lsp-ui-mode)

(setq-default TeX-master nil)
(add-hook 'LaTeX-mode-hook 'turn-on-cdlatex)   ; with AUCTeX LaTeX mode
(add-hook 'latex-mode-hook 'turn-on-cdlatex)   ; with Emacs latex mode
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)   ; with AUCTeX LaTeX mode
(add-hook 'latex-mode-hook 'turn-on-reftex)   ; with Emacs latex mode

(setq ediff-diff-options "")
(setq ediff-custom-diff-options "-u")
(setq ediff-window-setup-function 'ediff-setup-windows-plain)
(setq ediff-split-window-function 'split-window-vertically)
(autoload 'smerge-mode "smerge-mode" nil t)


;; Used by things like `ag` that use `read-directory' to display vertically.
;; (require 'ido-vertical-mode)
;; (ido-mode 1)
;; (ido-vertical-mode 1)
;; (setq ido-vertical-define-keys 'C-n-and-C-p-only)

(require 'company-prescient)
(company-prescient-mode 1)

(setq moody-mode-line-height 30)

(require 'helm-packages)

;; HTTP library for Emacs, uses curl as backend
(add-to-list 'load-path "~/emacs/packages/plz.el")
(require 'plz)

(add-to-list 'load-path "~/emacs/packages/code-compass")
(require 'code-compass)

(add-to-list 'load-path "~/emacs/packages/moldable-emacs")
(require 'moldable-emacs)
(me-setup-molds)

(require 'tycho-molds)

(setq plantuml-jar-path "~/Downloads/plantuml-1.2024.3.jar")
(setq plantuml-default-exec-mode 'jar)
(add-to-list 'auto-mode-alist '("\\.puml\\'" . plantuml-mode))
;;(add-to-list 'org-src-lang-modes '("plantuml" . plantuml))

;; Install tree-siter grammars:
;; (from https://www.masteringemacs.org/article/how-to-get-started-tree-sitter)
(setq treesit-language-source-alist
   '((bash "https://github.com/tree-sitter/tree-sitter-bash")
     (cmake "https://github.com/uyha/tree-sitter-cmake")
     (css "https://github.com/tree-sitter/tree-sitter-css")
     (elisp "https://github.com/Wilfred/tree-sitter-elisp")
     (go "https://github.com/tree-sitter/tree-sitter-go")
     (html "https://github.com/tree-sitter/tree-sitter-html")
     (java "https://github.com/tree-sitter/tree-sitter-java" "master" "src")
     (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
     (json "https://github.com/tree-sitter/tree-sitter-json")
     (kotlin "https://github.com/fwcd/tree-sitter-kotlin")
     (make "https://github.com/alemuller/tree-sitter-make")
     (markdown "https://github.com/ikatyang/tree-sitter-markdown")
     (python "https://github.com/tree-sitter/tree-sitter-python")
     (scala "https://github.com/tree-sitter/tree-sitter-scala")
     (toml "https://github.com/tree-sitter/tree-sitter-toml")
     (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
     (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
     (yaml "https://github.com/ikatyang/tree-sitter-yaml")))
(mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist))


(use-package tex-continuous
  :bind
  (:map LaTeX-mode-map
        ("C-c k" . tex-continuous-toggle)))
(use-package flymake
  :custom
  (flymake-show-diagnostics-at-end-of-line t)
  :bind
  (:map flymake-mode-map
        ("M-n" . flymake-goto-next-error)
        ("M-p" . flymake-goto-prev-error)))
