;; Deactivate linum-mode in org-mode
;; (add-hook 'org-mode-hook
;;           (lambda ()
;;             (global-linum-mode 0)))

(add-hook 'emacs-lisp-mode-hook 'highlight-coding-hook)
(add-hook 'ruby-mode-hook 'highlight-coding-hook)
(add-hook 'js2-mode-hook 'highlight-coding-hook)
;; (add-hook 'text-mode-hook 'turn-on-auto-fill)
;; (add-hook 'latex-mode-hook 'turn-on-auto-fill)

(add-hook 'visual-line-mode-hook #'visual-fill-column-mode)

;; Only turn on visual line mode if the buffer's name
;; ends with “.org” – this is to avoid visual line mode
;; messing up with Mold Org-mode output
(defun tycho/maybe-turn-on-visual-line-mode ()
  (let ((name (buffer-name (current-buffer))))
    (when (string-match ".*\.org$" name)
      (turn-on-visual-line-mode))))

(add-hook 'org-mode-hook 'tycho/maybe-turn-on-visual-line-mode)
(add-hook 'latex-mode-hook 'turn-on-visual-line-mode)


(defmacro rename-modeline (package-name mode new-name)
  `(eval-after-load ,package-name
     '(defadvice ,mode (after rename-modeline activate)
        (setq mode-name ,new-name))))

(rename-modeline "js2-mode" js2-mode "JS2")
(rename-modeline "clojure-mode" clojure-mode "Clj")

(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(add-hook 'latex-mode-hook 'turn-on-reftex)

;; (defun my-minibuffer-setup ()
;;   (set (make-local-variable 'face-remapping-alist)
;;        '((default :height 1.0))))
;; (add-hook 'minibuffer-setup-hook 'my-minibuffer-setup)

(setq projectile-switch-project-action 'helm-projectile)

(add-hook 'before-save-hook 'delete-trailing-whitespace)
(add-hook 'LaTeX-mode-hook 'turn-on-prettify-symbols-mode)

;; Use unzip for zip files in dired when pressing Z.
(eval-after-load "dired-aux"
  '(add-to-list 'dired-compress-file-suffixes
                '("\\.zip\\'" ".zip" "unzip")))


(add-hook 'after-init-hook
          (lambda ()
            (require 'server)
            (unless (server-running-p)
              (message "Starting emacs daemon...")
              (server-start))))

(add-hook 'ibuffer-hook
          (lambda ()
            (ibuffer-vc-set-filter-groups-by-vc-root)
            (unless (eq ibuffer-sorting-mode 'alphabetic)
              (ibuffer-do-sort-by-alphabetic))))

;; (add-hook 'dired-mode-hook
;;           (lambda () (dired-hide-details-mode +1)))

(add-hook 'prog-mode-hook
          (lambda () (setq-local display-line-numbers t)))
(add-hook 'groovy-mode-hook
          (lambda () (setq-local display-line-numbers t)))
(add-hook 'c-mode-common-hook
          (lambda () (setq-local display-line-numbers t)))
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(add-hook 'rust-mode-hook #'racer-mode)
(add-hook 'racer-mode-hook #'eldoc-mode)

;; Hook  emmet with SGML And CSS modes.

(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.

(defun my-clojure-mode-hook ()
    (clj-refactor-mode 1)
    (yas-minor-mode 1) ; for adding require/use/import statements
    ;; This choice of keybinding leaves cider-macroexpand-1 unbound
    (cljr-add-keybindings-with-prefix "C-c C-m"))

(add-hook 'clojure-mode-hook #'my-clojure-mode-hook)

;; Use lambda symbol
(add-hook 'emacs-lisp-mode-hook 'sm-greek-lambda)
(add-hook 'scheme-mode-hook 'sm-greek-lambda)

;; change "last modified" field in org mode files.
(add-hook 'before-save-hook #'zp/org-set-last-modified)
