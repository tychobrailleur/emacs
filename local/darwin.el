;; Must be getting blind in my old age
;; Allow hash to be entered
(global-set-key (kbd "M-3") #'(lambda () (interactive) (insert "#")))

;; Recent change causes weird encoding problems.
(set-locale-environment "utf-8")

(set-frame-font "Monaco 15" nil t)
(when window-system
  (maximize-frame))

(setq mac-option-key-is-meta t)
(setq mac-right-option-modifier nil)
(unless window-system
  (setq global-hl-line-mode nil))

(require 'simplify-mode)
(simplify-global-mode)

;; (load "auctex.el" nil t t)
;; (load "preview-latex.el" nil t t)

(add-to-list 'exec-path "/usr/local/bin")

(or (package-installed-p 'exec-path-from-shell) (package-install 'exec-path-from-shell))
(when window-system (exec-path-from-shell-initialize))

;; In terminal, the blue colour of minibuffer makes it impossible to read.
;; (unless (boundp 'aquamacs-version)
;;   (custom-set-faces
;;    '(minibuffer-prompt ((t (:foreground "yellow"))))))

(add-to-list 'load-path "/Users/sebastienlecallonnec/dev/emacs-chess")
(add-to-list 'load-path "/Users/sebastienlecallonnec/dev/mongo-el")
;;(add-to-list 'load-path "/Users/sebastienlecallonnec/dev/emacs-btc-ticker")

;; (setq TeX-view-program-selection
;;       (assq-delete-all 'output-pdf TeX-view-program-selection))

;;(require 'btc-ticker)
;;Optional: You can setup the fetch interval
;;default: 10 secs
;;(setq btc-ticker-api-poll-interval 30)

;;Enable btc-ticker-mode
;;(btc-ticker-mode 1)

;;(require 'org-confluence)

(require 'mongo)
(setf epa-pinentry-mode 'loopback)

;; Use spotlight as the locate command
(setq locate-command "mdfind")

(eval-after-load "TeX-mode"
  '(progn
     (setq TeX-view-program-selection '(((output-dvi style-pstricks) "dvips and gv")
                                        (output-dvi "xdvi")
                                        (output-pdf "open")
                                        (output-html "open")))))
