;; (require 'exwm)
;; (require 'exwm-config)
;; (require 'exwm-systemtray)

;; (exwm-config-default)
;; (exwm-systemtray-enable)

(defun exwm-logout ()
  (interactive)
  (recentf-save-list)
  (save-some-buffers)
  (start-process-shell-command "logout" nil "lxsession-logout"))


;; (load "auctex.el" nil t t)
;; (load "preview-latex.el" nil t t)

(require 'iso-transl)

(if (file-exists-p "email.el")
    (load "email.el" nil t t))

(unless window-system
  (setq global-hl-line-mode nil))


(defun fullscreen (&optional f)
  (interactive)
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                         '(2 "_NET_WM_STATE_MAXIMIZED_VERT" 0))
  (x-send-client-message nil 0 nil "_NET_WM_STATE" 32
                         '(2 "_NET_WM_STATE_MAXIMIZED_HORZ" 0)))

(when window-system (fullscreen))

(add-to-list 'load-path "~/dev/mongo-el")
(add-to-list 'load-path "~/dev/ebookify")

(require 'mongo)

;; (add-to-list 'load-path "~/dev/vala-mode")
;; (require 'vala-mode)

;; (require 'simplify-mode)
;; (simplify-global-mode)

;; sudo apt-get install sbcl on Ubuntu.
(setq inferior-lisp-program "sbcl")
(setq slime-contribs '(slime-fancy))
(load "/home/sebastien/quicklisp/clhs-use-local.el" t)

;; sudo apt-get install clisp on Ubuntu.
;; (setq inferior-lisp-program "clisp")
(setf epa-pinentry-mode 'loopback)
