(display-time)

;; Display path in titlebar
(setq frame-title-format
      (list (format "%s %%S: %%j " (system-name))
            '(buffer-file-name "%f" (dired-directory dired-directory "%b"))))

(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)
(setq uniquify-separator "/")
(setq uniquify-after-kill-buffer-p t) ; rename after killing uniquified
(setq uniquify-ignore-buffers-re "^\\*") ; don't muck with special buffers


(delete-selection-mode)
;; Set default font
;; (set-face-attribute 'default nil
;;                     :family "Droid Sans Mono"
;;                     :height 110
;;                     :weight 'normal
;;                     :width 'normal)

;; Yet another attempt at getting a fancy modeline
(setq-default powerline-default-separator 'wave
              powerline-height 20
              spaceline-highlight-face-func 'spaceline-highlight-face-modified
              spaceline-separator-dir-left '(left . left)
              spaceline-separator-dir-right '(right . right))


;; (require 'spaceline)
;; (require 'spaceline-config)

;; (spaceline-emacs-theme)
;; (spaceline-helm-mode)

;; from https://www.masteringemacs.org/article/hiding-replacing-modeline-strings
(defvar mode-line-cleaner-alist
  `((auto-complete-mode . " α")
    (yas/minor-mode . " υ")
    (yas-minor-mode . " υ")
    (company-mode . "")
    (paredit-mode . " π")
    (eldoc-mode . "")
    (abbrev-mode . "")
    (projectile-mode . "")
    (undo-tree-mode . "")
    (org-remark-mode . "🏷️")
    (org-remark-global-tracking-mode . "")
    (simplify-mode . " Σ")
    (visual-line-mode . " ⤶")
    (flyspell-mode . "")
    (fancy-dabbrev-mode . "")
    ;; Major modes
    (lisp-interaction-mode . "λ")
    (hi-lock-mode . "")
    (python-mode . "🐍")
    (java-mode . "☕️")
    (java-ts-mode . "☕️t")
    (emacs-lisp-mode . "EL")
    (org-mode . "🧠")
    (ruby-mode . "💎")
    (nxhtml-mode . "nx"))
  "Alist for `clean-mode-line'.

When you add a new element to the alist, keep in mind that you
must pass the correct minor/major mode symbol and a string you
want to use in the modeline *in lieu of* the original.")


(defun clean-mode-line ()
  (interactive)
  (cl-loop for cleaner in mode-line-cleaner-alist
        do (let* ((mode (car cleaner))
                  (mode-str (cdr cleaner))
                  (old-mode-str (cdr (assq mode minor-mode-alist))))
             (when old-mode-str
               (setcar old-mode-str mode-str))
             ;; major mode
             (when (eq mode major-mode)
               (setq mode-name mode-str)))))


(add-hook 'after-change-major-mode-hook 'clean-mode-line)

;;; alias the new `flymake-report-status-slim' to
;;; `flymake-report-status'
(defalias 'flymake-report-status 'flymake-report-status-slim)
(defun flymake-report-status-slim (e-w &optional status)
  "Show \"slim\" flymake status in mode line."
  (when e-w
    (setq flymake-mode-line-e-w e-w))
  (when status
    (setq flymake-mode-line-status status))
  (let* ((mode-line " Φ"))
    (when (> (length flymake-mode-line-e-w) 0)
      (setq mode-line (concat mode-line ":" flymake-mode-line-e-w)))
    (setq mode-line (concat mode-line flymake-mode-line-status))
    (setq flymake-mode-line mode-line)
    (force-mode-line-update)))
