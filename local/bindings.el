;;(global-set-key (kbd "C-x C-m") 'execute-extended-command)
(global-set-key (kbd "C-c C-m") 'execute-extended-command)
(global-set-key (kbd "C-w") 'backward-kill-word)
(global-set-key (kbd "C-x C-k") 'kill-region)
(global-set-key (kbd "C-c C-k") 'kill-region)

(global-set-key (kbd "M-/") 'hippie-expand)

(global-set-key (kbd "C-~") 'ag)

(global-set-key (kbd "C-x k") 'tycho/kill-this-buffer)
(global-set-key (kbd "C-x C-d") 'duplicate-line)

;;(global-set-key (kbd "M-[") 'insert-pair)
(global-set-key (kbd "M-\"") 'insert-pair)
(global-set-key (kbd "M-'") 'insert-pair)

;; Buffer menu, and go to other window
;;(global-set-key (kbd "C-x C-b") 'buffer-menu-other-window)

(global-set-key (kbd "C-x C-r") 'helm-recentf)
(global-set-key (kbd "C-x C-f") 'helm-find-files)

(global-set-key (kbd "C-x C-b") 'bufler-list)
(global-set-key (kbd "C-x b") 'helm-buffers-list)
(global-set-key (kbd "C-x y") 'helm-mini)

(global-set-key (kbd "C-x |") 'toggle-window-split)
(global-set-key (kbd "C-c |") 'col-highlight-flash)


;; Projectile bindings
;;  C-c p or s-p prefixes for Projectile, for example C-c p f to find file in project.
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)


(global-set-key (kbd "C-c SPC") 'ace-jump-mode)
(global-set-key (kbd "M-g c") 'avy-goto-char)
(global-set-key (kbd "M-g l") 'avy-goto-line)
(global-set-key (kbd "M-g w") 'avy-goto-word-1)

(global-set-key (kbd "C-x v H") 'vc-region-history)

;; Restore magit commit
;;(global-set-key (kbd "C-c C-c") 'magit-commit-commit)

;; iy-go-to-char after emacsrocks 4
;; (global-set-key (kbd "M-m") 'iy-go-to-char)
;; (global-set-key (kbd "C-M-m") 'iy-go-to-char-backward)
(global-set-key (kbd "C-=") 'er/expand-region)

(global-set-key (kbd "C-;") 'magit-status)
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-'") 'toggle-quotes)

;; (global-set-key (kbd "M-<up>") 'move-region-up)
;; (global-set-key (kbd "M-<down>") 'move-region-down)
(global-set-key (kbd "<C-return>") 'open-line-below)
(global-set-key (kbd "<C-S-return>") 'open-line-above)
(global-set-key (kbd "C-%") 'goto-match-paren)
(global-set-key (kbd "M-@") 'my-mark-current-word)
(global-set-key (kbd "M-*") 'pop-tag-mark)

(global-set-key (kbd "C-h j") 'javadoc-lookup)

(global-set-key (kbd "C-x n i") 'narrow-to-region-indirect)
(global-set-key (kbd "C-x C-j") 'dired-jump)

(global-set-key (kbd "C-n") 'next-logical-line)

(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)


(global-set-key (kbd "<f8>") 'elfeed)

(global-set-key (kbd "M-p") 'ace-window)
(global-set-key (kbd "C-c e") 'eval-and-replace)

(global-set-key (kbd "C-!") 'eshell-here)

;; Calendar
;; Fix foolish calendar-mode scrolling.
(add-hook 'calendar-load-hook
          #'(lambda ()
             (setq mark-holidays-in-calendar t)
             (define-key calendar-mode-map ">" 'scroll-calendar-left)
             (define-key calendar-mode-map "<" 'scroll-calendar-right)
             (define-key calendar-mode-map "\C-x>" 'scroll-calendar-left)
             (define-key calendar-mode-map "\C-x<" 'scroll-calendar-right)))

(add-to-list 'file-name-handler-alist '("\\.class$" . javap-handler))

(define-key company-active-map "[tab]" 'smarter-tab-to-complete)

;;smex
;; (global-set-key (kbd "M-x") 'counsel-M-x)
;;(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-x") 'helm-M-x)
;; (global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)


;; Ivy configuration
(setq ivy-use-virtual-buffers t)
;(setq enable-recursive-minibuffers t)
;;(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "C-c g") 'counsel-git)
(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c k") 'counsel-ag)
(global-set-key (kbd "C-c l") 'counsel-locate)
(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
(define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)

(global-set-key (kbd "M-]") 'elisp-refs-function)

(global-set-key (kbd "C-c m m") 'me/mold)
(global-set-key (kbd "C-c m f") 'me/go-forward)
(global-set-key (kbd "C-c m b") 'me/go-back)
(global-set-key (kbd "C-c m o") 'me/open-at-point)
(global-set-key (kbd "C-c m d") 'me/mold-docs)

(define-key minibuffer-local-completion-map (kbd "SPC") 'self-insert-command)
