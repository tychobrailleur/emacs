(setq debug-on-error nil)

(setq inhibit-startup-echo-area-message t)
(setq init-file-user (user-login-name))
(setq disabled-command-function nil)

(setq calendar-latitude 53.1)
(setq calendar-longitude -6.0)
(setq calendar-location-name "Greystones")

(setq calendar-time-zone 0)
