;; make frequently used commands short
(defalias 'qrr 'query-replace-regexp)
(defalias 'lml 'list-matching-lines)
(defalias 'dml 'delete-matching-lines)
(defalias 'dnml 'delete-non-matching-lines)
(defalias 'dtw 'delete-trailing-whitespace)
(defalias 'sl 'sort-lines)
(defalias 'rr 'reverse-region)
(defalias 'rs 'replace-string)

(defalias 'g 'grep)
(defalias 'rg 'rgrep)
(defalias 'gf 'grep-find)
(defalias 'fgd 'find-grep-dired)
(defalias 'fd 'find-dired)

(defalias 'rb 'revert-buffer)
(defalias 'swap 'rotate-windows)

(defalias 'sh 'shell)
(defalias 'ps 'powershell)
(defalias 'fb 'flyspell-buffer)
(defalias 'sbc 'set-background-color)
(defalias 'rof 'recentf-open-files)
(defalias 'lcd 'list-colors-display)
(defalias 'cc 'calc)

(defalias 'ff 'helm-find-files-in-project)

; elisp
(defalias 'eb 'eval-buffer)
(defalias 'er 'eval-region)
(defalias 'ed 'eval-defun)
(defalias 'eis 'elisp-index-search)
(defalias 'lf 'load-file)

; major modes
(defalias 'hm 'html-mode)
(defalias 'tm 'text-mode)
(defalias 'elm 'emacs-lisp-mode)
(defalias 'om 'org-mode)
(defalias 'ssm 'shell-script-mode)

; minor modes
(defalias 'wsm 'whitespace-mode)
(defalias 'gwsm 'global-whitespace-mode)
(defalias 'dsm 'desktop-save-mode)
(defalias 'acm 'auto-complete-mode)
(defalias 'vlm 'visual-line-mode)

;;(defalias 'list-buffers 'ibuffer)
(defalias 'list-buffers 'bufler)
(defalias 'package-list-packages 'helm-list-elisp-packages)


;; Character sets aliases
(defalias 'list-charsets 'list-character-sets)
(defalias 'describe-charset 'describe-character-set)

(defalias 'yes-or-no-p 'y-or-n-p)
