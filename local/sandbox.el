;; -*- lexical-binding: t -*-
;; Random stuff in Elisp.

(require 'seq)

;; text properties
(put-text-property
 (line-beginning-position)
 (line-end-position)
 'font-lock-face 'bold)

(defun clean-it ()
  (interactive)
  (remove-text-properties 119 140 '(font-lock-face face)))

(defun insert-foo-bar ()
  (interactive)
  (insert #("foo bar" 0 3 (face (:foreground "red")) 3 4 nil 4 7 (face italic))))

;; strings
(split-string "https://www.emacswiki.org/emacs/download/frame-fns.e" "/")

;; Trimming
(split-string " kdkdkdkfk  " nil nil "[[:blank:]]+")


; replace a regexp
(replace-regexp-in-string "fo+" "baz" "da foo is foobared")


;; mapcar
(mapcar
 (lambda (x) (* x x))
 (number-sequence 1 7 2))

;; Last arg of apply must be a list.
(apply #'+ 1 2 '(3 4))

;; Reduce applies function to two first args, then to result with next arg, etc.
(reduce #'+ '(1 2 3 4))

(cdr (car '((one one-and-a-half) two three)))

(mapcar
 (lambda (x) (split-string x " "))
 '("un petit" "exemple" "encore un autre"))

(seq-mapcat
 (lambda (x) (split-string x " "))
 '("un petit" "exemple" "encore un autre"))

;; join list
(mapconcat #'number-to-string '(1 2 3) ", ")
(string-join '("1" "2" "3") ", ")

;; member returns non nil if element is in list (using `equal').
;; it returns the sublist starting at element.
(member 4 '(1 2 3 4))
(member "arbre" '("arbre" "tronc" "branche")) ;; => '("arbre" "tronc" "branche"
(member "pouet" '("arbre" "tronc" "branche")) ;; nil


;; elnode
;; from http://nicferrier.github.io/elnode/
(defun my-elnode-hello-world-handler (httpcon)
  (elnode-http-start httpcon 200 '("Content-Type" . "text/html"))
  (elnode-http-return
   httpcon
   "<html><body><h1>Hello World</h1></body></html>"))
(elnode-start 'my-elnode-hello-world-handler :port 8028 :host "localhost")

(defun insert-uptime ()
  (interactive "iP")
  (emacs-uptime))

;; filenotify example.
(require 'filenotify)
(defun example (event) (message "Stuff!"))
(file-notify-add-watch "~/.emacs" '(change) #'example)

;; Timer.
(defvar counter 0)
(defun print-message ()
  (setq counter (1+ counter))
  (message "Hello World %s %s" counter (format-time-string "%H:%M:%S.%3N")))
(run-with-timer 20 20 'print-message)
(cancel-timer (car timer-list))

(mapcar 'cancel-timer timer-list)

;; nconc
;; concatenate lists by altering them, only last one is not altered.
(let ((val '(1 2 3)))
  (if val (nconc val '(4 5)))
  val)

(setq x '(1 2 3))
(nconc x 'z)
x ; => (1 2 3 . z)

(require 'dash)

(-interpose "--" '(1 2 3))

(defun ask-for-code (code)
  (interactive "MMerchant: ")
  (let ((code-buffer (generate-new-buffer "*Code*")))
    (set-buffer "*Code*")
    (insert (concat "SELECT * FROM my_details where id = '" code "';"))))

(dolist (var '("one" "two" "three"))
  (message (upcase var)))

;; append elements to list

(let ((acc '()))
  (dolist (var '(1 2 3 4))
    (setq acc (cons var acc)))
  acc)

;; setf

(let ((val '(1 2 3 4)))
  (setf (cadr val) 7)
  val)
;; => (1 7 3 4)

;; letf

(setq my-list '(1 2 3 4 5 6))
(cl-letf (((car my-list) 1000)
          ((elt my-list 3) 200))
  (message "%s" my-list)) ;; => (1000 2 200 4 5 6)

(message "%s" my-list) ;; => (1 2 3 4 5 6)

;; cl-loop
(cl-loop for i in '(1 2 3) collect (* i i))

                                        ; project euler 1
(cl-loop for y from 1 to 999
         when (or (zerop (% y 3))(zerop (% y 5)))
         sum y)
                                        ; project euler 2
(cl-loop for x = 0 then y
         for y = 1 then z
         for z = 2 then (+ x y)
         until (> z 4000000)
         when (evenp z)
         sum z)

(cl-loop for (first . second) in '(("a" . 1) ("b" . 4) ("c" . 42))
         do (message (concat "Hello " first " " (number-to-string second))))

;; Seq
(setq my-list '(1 2 3 4))
(seq-doseq (i my-list) (print (* i 2)))
(seq-map #'(lambda (x) (* x x)) my-list) ;;=> (1 4 9 16)
(seq-reduce #'+ my-list 0) ;;=> 10

(concat (seq-map #'upcase "abcd"));; => "ABCD"   -- eval-print-last-sexp


;; Association lists
(setq my-assoc-list '(("one" . "un") ("two" . "deux") ("three" . "trois")))

(message (format "1: %s" (alist-get "one" my-assoc-list)))     ;; => 1: nil
(message (format "1: %s" (assoc "one" my-assoc-list)))         ;; => 1: (one . un)
(message (format "1: %s" (assoc-default "one" my-assoc-list))) ;; => 1: un
(message (format "1: %s" (assq "one" my-assoc-list)))          ;; => 1: nil


(setq ages '((mary . 23)
             (john . 24)
             (smith . 33)))

(assq 'mary ages)
(rassq 23 ages)
;; Also...

(setq my-list (list :a 1 :b 2 :c 3))
(getf my-list :a)
(getf my-list :c)


;; concatenate args into a vector.
(vconcat (list 1 2 3)
         [8]
         (list '(4 5) 6))

;; *var* is a convention for a global variable

(defvar *db* nil)
(defun make-cd (title artist rating ripped)
  (list :title title :artist artist :rating rating :ripped ripped))
(defun add-record (cd)
  (push cd *db*))
(add-record (make-cd "Fly" "dixie Chicks" 7 t))

(defun dump-db ()
  (dolist (cd *db*)
    (format "%s" cd)))

;; dictionaries
(setq my-hash (make-hash-table :test 'equal))
(puthash "one" "un" my-hash)
(puthash "two" "deux" my-hash)
(puthash "three" "trois" my-hash)

(message (format "2: %s" (gethash "two" my-hash)))

;; Using map.el interface
(map-put my-hash "one" "uno")
(message (format "1: %s" (map-elt my-hash "one")))


;; plist (property list) is a list of paired elements

(setq plist-example '(:bar 1 :foo 4))
(setq plist-example (plist-put plist-example :foo 69))
(cl-destructuring-bind (&key bar foo &allow-other-keys) plist-example
  (+ bar foo))


;; pcase
;; Example of pcase
(defun fib (n)
  (pcase n
    (`0 1)
    (`1 1)
    (n (+ (fib (1- n)) (fib (- n 2))))))
(mapcar #'fib '(1 2 3 4 5 6))


(defun match-pair (p)
  (pcase p
    (`(,x . ,y) (message " -> x: %s, y: %s" x y))
    (_ (error "No such pattern"))))

(match-pair '("a" . "5"))
(match-pair 4)

(pcase (list 1 2 3)
  (`(,a ,b ,c) (+ a b c)))

(cl-destructuring-bind (a b c) (list 1 2 3)
  (+ a b c))

(pcase (list 1 2 3 4)
  (`(,a ,_ ,_ ,d) (+ a d)))

(pcase (list 1 2 "Pouet" 3)
  (`(1 2 ,foo 3)
   (message (format "String is %s" foo))))


(pcase-let ((`(,a ,b ,c) (list 1 2))) (list a b c))

(pcase (read-answer "Which? "
                    '(("all" ?a "All")
                      ("yes" ?y "Yes")))
  ("all" (message "Pouet"))
  ("yes" (message "good choice")))


;; cl-defun

(cl-defun my-paramed-func (arg1 &key (arg2 "pouet"))
  (message (format "arg1: %s, arg2: %s" arg1 arg2)))
(my-paramed-func "one") ;; displays "arg1: one, arg2: pouet

;; Macros

(defmacro incl (var)
  `(setq ,var (1+ ,var)))

(macroexpand '(incl r))


;; Make UI

(defface sandbox-title-face
  '((t (:inherit font-lock-type-face :foreground "orange2" :weight bold)))
  "Lovely new face")

(defun sandbox-create-something ()
  (interactive)
  (let ((a-new-buffer (get-buffer-create "*Something*"))
	    (inhibit-read-only t))
    (with-current-buffer a-new-buffer
      (erase-buffer)
      (insert (propertize "  Something – An exciting new menu" 'face 'sandbox-title-face))
      (pop-to-buffer (current-buffer))
      (setq buffer-read-only t))))

;; buffers

(let ((buf (get-buffer-create "*New Cool Buffer*")))
   (with-current-buffer buf
      (insert (propertize "  Something – An exciting new menu" 'face 'sandbox-title-face)))
    (switch-to-buffer buf))


;; save-excursion
;;  It saves the location of point, executes the body of the function,
;; and then restores point to its previous position if its location was changed.

(let ((position 42))
      (save-excursion
        (goto-char position)
        (forward-line 0)))

;; Make sure font-lock-mode is disabled before running forms below.
;; font-lock-mode

(insert (propertize "foo" 'face 'italic
                    'mouse-face 'bold-italic))

(insert (propertize "Yeah!!" 'face 'outline-1))

;; Write in file


(defvar file-to-process "~/dev/fuelable/payments-web/grails-app/conf/BuildConfig.groovy")
(setq file-to-process "~/dev/fuelable/payments-web/grails-app/conf/BuildConfig.groovy")

(with-temp-file (expand-file-name file-to-process)
  (save-excursion
    (if (file-exists-p file-to-process)
        (insert-file-contents file-to-process))
    (goto-char 1)
    (while (re-search-forward "com.simplify:payments-common:\\([^'\"]+\\)" nil t)
      (replace-match "10.0.0" nil nil nil 1))))



;; Directory and file handling

(expand-file-name "./sandbox.el")
(file-name-directory (directory-file-name (expand-file-name "./sandbox.el")))
(file-name-as-directory "./sanbox.el")
(locate-dominating-file (expand-file-name "./sandbox.el") ".git")
(locate-dominating-file (expand-file-name "~/dev/emacs/lisp/org/")
                        (lambda (parent) (directory-files parent nil "\\(GNU\\)?[Mm]akefile"))) ;; ???

; list directory files
(directory-files
 (expand-file-name "~/dev/HO/src/main/resources/sprache")
 t
 "\\.properties$")

;; pattern to process files in batch
(setq property-files (directory-files
 (expand-file-name "~/dev/HO/src/main/resources/sprache")
 t
 "\\.properties$"))
(mapc
  (lambda (f)
    (let ((cur-buffer (find-file f)))
      (goto-char (point-max))
      (insert "pouet")
      (save-buffer)
      (kill-buffer cur-buffer)))
  property-files)

;; can also use `find-lisp'

(require 'find-lisp)
(find-lisp-find-files (expand-file-name "~/dev/HO/src/main/resources/sprache") "\\.properties$")

;; List files in a directory, and print out a YAML extract with them:
(require 's)

(defun print-resources (prefix resource)
  (let ((path (s-replace prefix "" resource)))
    (concat "  - source: " path "\n     destination: " path "\n")))

(let* ((prefix (expand-file-name
                "~/dev/"))
       (files (find-lisp-find-files prefix "\\..*$")))
  (mapconcat (lambda (f) (print-resources prefix f)) files))



;; Replace string in a file with value, and write to a temp file

(with-temp-file (expand-file-name "/tmp/that.txt")
  (insert-file-contents (expand-file-name "~/dev/org-chess/chessboard.tmpl"))
  (message (buffer-string))
  (set-buffer-file-coding-system 'utf-8)
  (goto-char 1)
  (while (re-search-forward "\\(#fen#\\)" nil t)
    (replace-match "8/pppr1kpp/8/8/8/5P2/PPP1RKPP/8 w - - 0 1" nil nil nil 1)))

;; tabulated lists

(define-derived-mode mymode tabulated-list-mode "mymode" "Major mode My Mode, just a test"
  (setq tabulated-list-format [("Col1" 18 t)
                               ("Col2" 12 nil)
                               ("Col3"  10 t)
                               ("Col4" 0 nil)])
  (setq tabulated-list-padding 2)
  (setq tabulated-list-sort-key (cons "Col3" nil))
  (tabulated-list-init-header))

(defun print-current-line-id ()
  (interactive)
  (message (concat "current line ID is: " (tabulated-list-get-id))))

(defun my-listing-command ()
  (interactive)
  (pop-to-buffer "*MY MODE*" nil)
  (mymode)
  (setq tabulated-list-entries (list
                                (list "1" ["1" "2" "3" "4"])
                                (list "2" ["a" "b" "c" "d"])))
  (tabulated-list-print t))


(let ((data-buffer
       (url-retrieve-synchronously "https://failteireland.azure-api.net/opendata-api/v1/attractions")))
  (switch-to-buffer data-buffer))


;;  Networking

(let ((proc (make-network-process
             :name "ssdp"
             :buffer "*uPnP*"
             :host "239.255.255.250"
             :type 'datagram
             :family 'ipv4
             :service 1900)))
  (process-send-string proc "M-SEARCH * HTTP/1.1\r
ST:ssdp:all\r
MAN:\"ssdp:discover\"\r
Host:239.255.255.250:1900\r
MX:5\r
\r\n\r\n")
  (switch-to-buffer "*uPnP*"))

(delete-process "ssdp")

(request
 "http://httpbin.org/post"
 :type "POST"
 :data '(("key" . "value") ("key2" . "value2"))
 :parser 'json-read
 :success (function*
           (lambda (&key data &allow-other-keys)
             (message "I sent: %S" (assoc-default 'form data)))))


(request "https://api.test.kount.net/kc/getThresholds.json"
         :headers '(("Content-Type" . "application/x-www-form-urlencoded")
                    ("X-Kount-Api-Key" . ,(getenv "KOUNT_API_KEY")))
         :type "POST"
         :data "{\"merchantId\": 100001, \"customerId\": \"219b0f74277f4a3395d3f6eea82776d8\"}"
         :parser 'json-read
         :success (function*
                   (lambda (&key data &allow-other-keys)
                     (with-current-buffer (get-buffer-create "nicfeed")
                       (goto-char (point-max))
                       (insert (format "Message: %s" (assoc-default 'reply data))))))
         :error (function*
                 (lambda (&key data &allow-other-keys)
                   (message "Error: %S" (assoc-default 'form data)))))


(defun get-lichess-user (username)
  (let ((user-details nil))
    (request (format "https://lichess.org/api/user/%s" username)
             :headers '(("Content-Type" . "application/json"))
             :type "GET"
             :parser 'json-read
             :success (function*
                       (lambda (&key data &allow-other-keys)
                         (setq user-details data))))
    (message "user-details = %s" user-details)
    user-details))

(get-lichess-user "crankyeructation")


;; Call JIRA Rest API
;; Cf. https://docs.atlassian.com/jira/REST/cloud/ for more details.
(request "https://labs.mastercard.com/jira/rest/api/latest/issue/MAPPS-9070?expand=names,renderedFields"
         :headers '(("Content-Type" . "application/json"))
         :type "GET"
         :parser 'json-read
         :success (function*
                   (lambda (&key data &allow-other-keys)
                     (with-current-buffer (get-buffer-create "*output*")
                       (goto-char (point-max))
                       (insert (format "Message: %s" data)))))
         :error (function*
                 (lambda (&key data &allow-other-keys)
                   (message "Error: %S" data))))


;; Create a frame
(make-frame '((border . 0)
              (title . "My Frame")))


;; Create a new frame
(make-frame '((title . "Hello")
              (height . 20)
              (border-width . 0)))



;; run-with-idle-timer

(run-with-idle-timer 1 nil (lambda () (message "%s" (syntax-table))))

(run-with-idle-timer 1 nil
                     (lambda () (message "%s %s"
                                         (current-buffer)
                                         (eq (syntax-table)
                                             emacs-lisp-mode-syntax-table))))

;; if-let* / when-let*

(let ((x nil))
  (if-let (x)
      "then"
    "else"))


;; struct

(cl-defstruct example name code version)
(setq my-struct-example (make-example))
(setf (example-name my-struct-example) "First Example")
(message (format "Example name:%s" (example-name my-struct-example)))


;; See Also
;; http://wikemacs.org/wiki/Emacs_Lisp_Cookbook

(defun adjust-font-size (start end)
  "Adjust the font size"
  (interactive "r")
  (let* ((current-size (string-to-number (buffer-substring start end)))
         (new-size nil))
    (setq new-size (floor (/ (* 11.0 current-size) 14.0)))
    (delete-region start end)
    (insert (number-to-string new-size))))


;; AVL Trees
(require 'avl-tree)

(defun integer-compare (a b)
  (< a b))

(setq tree (avl-tree-create #'integer-compare))

(avl-tree-enter tree 3)
(avl-tree-enter tree 1)
(avl-tree-enter tree 2)
(avl-tree-enter tree 4)
(avl-tree-enter tree 6)
(avl-tree-enter tree 5)

(avl-tree-delete tree 4)

(print tree)


;;  Threads
;;


;; create a thread
(make-thread (lambda () (dotimes (index 20)
                          (progn
                            (message (format "Beep #%s" index))
                            (sleep-for 2)))) "count-thread")

;; List all threads
(all-threads)


(make-frame `((left . ,(+ (car (frame-position)) (frame-outer-width)))
	          (top . ,(cdr (frame-position)))))


;; Org handling

(with-temp-buffer
  (insert-file-contents "~/org/notes.org")
  (org-element-map (org-element-parse-buffer) 'keyword #'identity))

;; Processes

(with-output-to-string
  (call-process "xdg-user-dir" nil standard-output nil "DOWNLOAD"))

;;

;; list of all suffixes

(defun list-suffixes (list)
  (cond
   ((null list) '())
   (t (cons list (list-suffixes (cdr list))))))

(list-suffixes '(1 2 3 4))

(defun list-all-pairs (list all-pairs)
  (let ((head (car list)))
    (if (= (length list) 1)
        all-pairs
      (progn
        (dolist (var (cdr list))
          (setq all-pairs (cons (cons head var) all-pairs)))
        (list-all-pairs (cdr list) all-pairs)))))


(defun factorial (n)
  (if (> n 1)
      (* n (factorial (1- n)))
    1))


;; button
; the below only works if font-lock mode is disabled.
(insert-text-button "pouet"
                    'follow-link t
                    'face 'apropos-function-button
                    'action (lambda (_) (eww-browse-url "https://ga.wikipedia.org")))

(setq pouet-label (propertize "pouet" 'face 'apropos-function-button))
(insert-text-button pouet-label
                    'follow-link t
                    'action (lambda (_) (eww-browse-url "https://ga.wikipedia.org")))

;; in font-lock mode, `face' will not work, as font-lock highlighting overrides it.

(insert (propertize "error" 'face 'apropos-function-button))
(insert (propertize "error" 'font-lock-face 'apropos-function-button))

(define-widget 'link 'item
  "An embedded link."
  :button-prefix 'widget-link-prefix
  :button-suffix 'widget-link-suffix
  :follow-link "\C-m"
  :help-echo "Follow the link."
  :format "%[%t%]")


;; memoization
;;(require 'memoize)

;; (defmemoize fib (n)
;;   (cond ((= n 0) 0)
;; 	    ((= n 1) 1)
;; 	    (t (+ (fib (- n 1))
;; 	          (fib (- n 2))))))


;; may need to adjust max-lisp-eval-depth
;;(fib 400) ;;=>  176023680645013966468226945392411250770384383304492191886725992896575345044216019675


;; plz.el

(plz 'get "https://httpbin.org/user-agent")

(plz 'get "https://httpbin.org/get" :as #'json-read)

(plz 'post "https://httpbin.org/post"
  :headers '(("Content-Type" . "application/json"))
  :body (json-encode '(("key" . "value")))
  :as #'json-read
  :then (lambda (alist)
          (message "Result: %s" (alist-get 'data alist))))

(let ((jpeg-data (plz 'get "https://httpbin.org/image/jpeg" :as 'binary)))
  (create-image jpeg-data nil 'data))

;; Experiments with rx


(require 's)
(require 'rx)
(require 'pcre2el)
(let ((rr (rx (group "\"" (zero-or-more (or (not (any "\""))
                                            (seq (one-or-more "\\") "\"")))
                     "\""))))
  (message (rxt-elisp-to-pcre rr))
  (s-matches-p rr "\"hello\", or \"hello, \\\"world\\\"\" or \"world\" and something else or \"something else\""))

;; examples of using deferred
(require 'deferred)

(deferred:$
 (deferred:url-retrieve "http://universities.hipolabs.com/search?country=Ireland")
 (deferred:nextc it (lambda (buf)
                      (with-current-buffer buf
                        (goto-char (point-min))
                        (switch-to-buffer buf)))))


;; multibyte experiments
(string-to-multibyte (format "%c" #x2764))

;; List buffers and perform action on buffers with major mode

(dolist (current-buffer (buffer-list (current-buffer)))
  (with-current-buffer current-buffer
    (message "Buffer: %s" (buffer-name current-buffer))
    (when helm-mode
      (message "x %s" current-buffer))))


;; Re-generate autoloads
(package-generate-autoloads "jenkinsfile-mode" "~/.emacs.d/elpa/jenkinsfile-mode-20220414.542")


;; programmed completion
(completing-read "Select: "
                 ;; string to ne co,pleted, a predicate function to filter
                 ;; a flag specifying type of completion
                 (lambda (string predicate action)
                   (if (eq action 'metadata)
                       (message "s: %s, p: %s, a: %s" string predicate action)
                     '("a" "b" "c" "d" "e"))))

;; Example of use of the helm interface
;; See also https://rnchzn.github.io/helm/doc/helm-devel.html
;; for more details.
(helm :sources
      (list (helm-build-sync-source "Greetings!"
              :candidates '("Hej" "Hello" "Hallo" "Bonjour")))
      :input (word-at-point)
      :prompt "Which one? ")

;; example of use of transient
(require 'transient)
(transient-define-prefix my-transient-main-menu ()
  "My Transient main menu."
  [["Tycho"
    :pad-keys t
    ("&" "😊" #'(lambda (_) (insert "😊")) :transient nil)
    ("Q" "🥺" #'(lambda (_) (insert "🥺")) :transient nil)
    ("n" "😱" #'(lambda (_) (insert "😱")) :transient nil)
    ("^" "🤤" #'(lambda (_) (insert "🤤")) :transient nil)
    ("=" "⭐" #'(lambda (_) (insert "⭐")) :transient nil)]]

  [:class transient-row
          ;; Note: no need to C-g for main menu
          ("q" "Dismiss" ignore :transient transient--do-exit)])
