;;(require 'org-feed)
(require 'org-tempo)
(require 'org-journal)
(require 'org-journal-list)
;;(require 'ox-confluence) ;; Export to Confluence markup using  `org-confluence-export-as-confluence'
(require 'org-ref)
(require 'org-ref-helm)

(setq org-directory "~/org"
      org-default-notes-file "~/org/notes.org"
      org-image-actual-width 850
      org-agenda-compact-blocks nil
      org-agenda-block-separator 9472
      org-extend-today-until 5)

(with-eval-after-load 'org (global-org-modern-mode))
(use-package citar
  :custom
  (citar-bibliography '("~/org/org-roam/privacy.bib"))
  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup))

(global-set-key (kbd "C-c c") #'org-capture)
(global-set-key (kbd "C-c a") #'org-agenda)

;; TODO look into https://github.com/progfolio/doct
;; Format of each template in org-capture-templates is:
;; (keys
;;  description
;;  type (can be entry, item, checkitem, table-line, or plain)
;;  target
;;  template
;;  ...
;; )
;; Rest of the entries are pre-defined properties
;; that can be found in the docs: https://orgmode.org/manual/Template-elements.html

(setq org-capture-templates
      '(("t"
         "Todo List Item"
         entry
         (file+headline "~/org/todo.org" "Tasks")
         "* TODO %?\n  %i\n  %a")
        ("f"
         "Fleeting"
         entry
         (file "~/org/org-roam/inbox.org")
         "* %?\n"
         :empty-lines-before 1)
        ("j"
         "Journal Entry"
         entry
         (file+olp+datetree "~/org/org-roam/journal.org")
         (file "~/emacs/templates/journal.orgcaptmpl"))))

;; Format for org-roam-capture-templates is different;
;; in particular it requires a `:target` property
;; C-h v org-roam-capture-templates for more info.
(setq org-roam-capture-templates
      '(("d"
         "Default"
         plain
         "%?"
         :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                            "#+title: ${title}
#+author: Sébastien Le Callonnec
#+created: %u
#+last_modified: %U\n\n") :unnarrowed t)
        ;; Attempt at getting into a proper Zettelkasten workflow
        ;; Fleeting notes are the temporary notes that will processed
        ;; (created using classic org-capture for quick insert into inbox.org),
        ;; and turned into a permanent note (“Zettel”) in `main`, or just discarded.
        ;; For now just `main' and `reference'
        ;; Other TODO are: `articles' & `vendors' – still need to devise the workflow.
        ("m"
         "Main"
         plain
         "%?"
         :target (file+head "~/org/org-roam/main/%<%Y%m%d%H%M%S>-${slug}.org"
                            "#+title: ${title}
#+author: Sébastien Le Callonnec
#+created: %u
#+last_modified: %U\n\n")
         :unnarrowed t)
        ("r"
         "Reference"
         plain
         "%?"
         :target (file+head "~/org/org-roam/reference/%<%Y%m%d%H%M%S>-${slug}.org"
                            "#+title: ${title}
#+created: %u
#+last_modified: %U\n\n")
         :unnarrowed t)))

;; org-journal config
(setq org-journal-dir "~/org/journal/")
(setq org-journal-date-format "%A, %d %B %Y")
(require 'org-journal)


;; Prevent electric pair for “<” which is used by org-tempo for templating.
;; Cf. https://www.topbug.net/blog/2016/09/29/emacs-disable-certain-pairs-for-electric-pair-mode/
(add-hook
 'org-mode-hook
 (lambda ()
   (setq-local electric-pair-inhibit-predicate
               `(lambda (c)
                  (if (char-equal c ?<) t (,electric-pair-inhibit-predicate c))))))

(setq org-feed-alist '(("CTAN Announcements"
                        "https://ctan.org/ctan-ann/rss"
                        "~/org/notes-perso.org"
                        "CTAN Announcements")))

;;(add-hook 'org-mode-hook 'turn-on-flyspell)
(add-hook 'org-mode-hook (lambda () (fancy-dabbrev-mode)))
(add-hook 'org-mode-hook (lambda () (focus-mode)))

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "~/org/org-roam/"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c z"   . org-roam-capture)
         ("C-c n c" . org-roam-capture) ;; whatever feels more appropriate in the context
         ("C-c n b" . org-roam-switch-to-buffer)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag))
        org-roam-buffer-position 'right
        org-roam-index-file "~/org/org-roam/index.org")
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol))

;;  It looks like messing with org-roam-node-display-template breaks
;;  ability to find nodes when doing org-roam-node-insert
(with-eval-after-load 'org-roam
  (cl-defmethod org-roam-node-type ((node org-roam-node))
    "Return the TYPE of NODE.

This then can be used in the `org-roam-node-display-template' template
by using `${type:...}'.
"
    (condition-case nil
        (file-name-nondirectory
         (directory-file-name
          (file-name-directory
           (file-relative-name (org-roam-node-file node) org-roam-directory))))
      (error ""))))

(require 'org-download)

;; Drag-and-drop to `dired`
(add-hook 'dired-mode-hook 'org-download-enable)
(setq-default org-download-image-dir "~/org/images")
(setq org-image-actual-width nil)

;; Configuration in support of org-modern use
;; Still on the fence about this.
(setq org-auto-align-tags nil
      org-tags-column 0
      org-catch-invisible-edits 'show-and-error
      org-special-ctrl-a/e t
      org-insert-heading-respect-content t

      ;; Org styling, hide markup etc.
      org-hide-emphasis-markers t
 ;;;;;     org-pretty-entities t ;;;; NO NO NO
      org-ellipsis "…"

      ;; Agenda styling
      org-agenda-tags-column 0
      org-agenda-block-separator ?─
      org-agenda-time-grid
      '((daily today require-timed)
        (800 1000 1200 1400 1600 1800 2000)
        " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
      org-agenda-current-time-string
      "◀── now ─────────────────────────────────────────────────")

(org-remark-global-tracking-mode +1)
(define-key global-map (kbd "C-c n m") #'org-remark-mark)

;; The rest of keybidings are done only on loading `org-remark'
(with-eval-after-load 'org-remark
  (define-key org-remark-mode-map (kbd "C-c n o") #'org-remark-open)
  (define-key org-remark-mode-map (kbd "C-c n ]") #'org-remark-view-next)
  (define-key org-remark-mode-map (kbd "C-c n [") #'org-remark-view-prev)
  (define-key org-remark-mode-map (kbd "C-c n r") #'org-remark-remove))

(add-to-list 'load-path "~/emacs/packages/helm-org-walk")
(add-to-list 'load-path "~/emacs/packages/roam-with-helm")

(require 'roam-with-helm-v2)
(define-key global-map (kbd "C-c f") 'helm-org-roam)
