;; from http://eden.rutgers.edu/~mangesh/emacs.html
;; Frame title bar formatting to show full path of file
(setq-default
 frame-title-format
 (list '((buffer-file-name " %f" (dired-directory
                                  dired-directory
                                  (revert-buffer-function " %b"
                                                          ("%b - Dir:  " default-directory)))))))

(setq-default
 icon-title-format
 (list '((buffer-file-name " %f" (dired-directory
                                  dired-directory
                                  (revert-buffer-function " %b"
                                                          ("%b - Dir:  " default-directory)))))))

(defun switch-to-minibuffer-window ()
  "switch to minibuffer window (if active)"
  (interactive)
  (when (active-minibuffer-window)
    (select-window (active-minibuffer-window))))

(global-set-key (kbd "<f6>") 'switch-to-minibuffer-window)

(defun java-mode-untabify ()
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "[ \t]+$" nil t)
      (delete-region (match-beginning 0) (match-end 0)))
    (goto-char (point-min))
    (if (search-forward "\t" nil t)
        (untabify (1- (point)) (point-max))))
  nil)

(defun format-deadlock ()
  (interactive)
  "format the InnoDB monitor status log"
  (save-excursion
    (goto-char (point-min))
    (while (search-forward "\\n" nil t)
      (replace-match "\n"))))

(defun xml-prettify ()
  (interactive)
  (save-excursion
    (shell-command-on-region (point-min) (point-max) "xmllint --format -" (buffer-name) t)
    (nxml-mode)
    (indent-region (point-min) (point-max))))

;; (add-hook 'groovy-mode-hook
;;           #'(lambda ()
;;               (progn
;;                 ;; Indent labels for groovy to ensure spock labels
;;                 ;; are correctly indented.
;;                 (c-add-style "groovy" '("java"
;;                                         (c-echo-syntactic-information-p . t)
;;                                         (c-comment-only-line-offset . (0 . 0))
;;                                         (c-offsets-alist . ((c                     . c-lineup-C-comments)
;;                                                             (statement-case-open   . 0)
;;                                                             (case-label            . +)
;;                                                             (substatement-open     . 0)))))
;;                 (c-set-style "groovy"))))

(defun valgrind ()
  (interactive)
  (compilation-minor-mode)
  (define-key compilation-minor-mode-map (kbd "")'comint-send-input)
  (define-key compilation-minor-mode-map (kbd "S-")'compile-goto-error))

(defun copy-file-path ()
  (interactive)
  (kill-new (buffer-file-name)))

;; (add-hook 'shell-mode-hook 'valgrind)

;; source: http://steve.yegge.googlepages.com/my-dot-emacs-file
(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file name new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))

;; source:: https://sites.google.com/site/steveyegge2/my-dot-emacs-file
(defun move-buffer-file (dir)
  "Moves both current buffer and file it's visiting to DIR."
  (interactive "DNew directory: ")
  (let* ((name (buffer-name))
         (filename (buffer-file-name))
         (dir
          (if (string-match dir "\\(?:/\\|\\\\)$")
              (substring dir 0 -1) dir))
         (newname (concat dir "/" name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (progn
        (copy-file filename newname 1)
        (delete-file filename)
        (set-visited-file-name newname)
        (set-buffer-modified-p nil) t))))

;; source: http://tuxicity.se/emacs/elisp/2010/11/16/delete-file-and-buffer-in-emacs.html
(defun delete-file-and-buffer ()
  "Removes file connected to current buffer and kills buffer."
  (interactive)
  (let ((filename (buffer-file-name))
        (buffer (current-buffer))
        (name (buffer-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (when (yes-or-no-p "Are you sure you want to remove this file? ")
        (delete-file filename)
        (kill-buffer buffer)
        (message "File '%s' successfully removed" filename)))))

(defun comment-or-uncomment-region-or-line ()
  "Comments or uncomments the region or the current line if there's no active region."
  (interactive)
  (let (beg end)
    (if (region-active-p)
        (setq beg (region-beginning) end (region-end))
      (setq beg (line-beginning-position) end (line-end-position)))
    (comment-or-uncomment-region beg end)
    (next-line)))

;; Some useful aliases to support my common mistyped commands.
(defalias 'imeny 'imenu)
(defalias 'qck 'ack)


;; Ctags
;; from: http://www.emacswiki.org/emacs/BuildTags#toc2
(setq path-to-ctags "/usr/local/bin/ctags")
(defun create-tags (dir-name)
  "Create tags file."
  (interactive "DDirectory: ")
  (shell-command
   (format "%s -e --recurse -f %s/TAGS -R %s/*" path-to-ctags dir-name (directory-file-name dir-name))))

(cl-defun delete-second-line (&optional (count 1))
  (interactive "p")
  (beginning-of-line)
  (forward-line)
  (kill-line)
  (when (> count 1)
    (delete-second-line (1- count))))

(defun macos-copy ()
  (interactive)
  (shell-command-on-region (region-beginning) (region-end) "pbcopy"))

(defun cljs-repl ()
  (interactive)
  (setq inferior-lisp-program "cljs-repl")
  (run-lisp))

;; From http://www.emacswiki.org/emacs/TransposeWindows
(defun rotate-windows ()
  "Rotate your windows"
  (interactive)
  (cond
   ((not (> (count-windows) 1))
    (message "You can't rotate a single window!"))
   (t
    (let ((i 0)
          (num-windows (count-windows)))
      (while  (< i (- num-windows 1))
        (let* ((w1 (elt (window-list) i))
               (w2 (elt (window-list) (% (+ i 1) num-windows)))
               (b1 (window-buffer w1))
               (b2 (window-buffer w2))
               (s1 (window-start w1))
               (s2 (window-start w2)))
          (set-window-buffer w1 b2)
          (set-window-buffer w2 b1)
          (set-window-start w1 s2)
          (set-window-start w2 s1)
          (setq i (1+ i))))))))

(defun ido-recentf-open ()
  "Use `ido-completing-read' to \\[find-file] a recent file"
  (interactive)
  (if (find-file (ido-completing-read "Find recent file: " recentf-list))
      (message "Opening file...")
    (message "Aborting")))

(defun highlight-coding-hook ()
  (make-local-variable 'column-number-mode)
  (column-number-mode t)
  (show-paren-mode t)
  (if window-system (hl-line-mode t))
  (show-paren-mode t)
  (idle-highlight-mode t))

(defun lower-camel-case (arg)
  "Change a string of characters into camel case."
  (interactive)
  (downcase (replace-regexp-in-string " " "_" arg)))

(defun word-count ()
  (interactive)
  (shell-command-on-region (point-min) (point-max) "wc" (minibuffer) t))

(defun latex-word-count ()
  (interactive)
  (shell-command-on-region (point-min) (point-max) "sed -ne '/\\\\begin{document}/,/\\\\end{document}/p' | detex | wc -w"))

;; Kill rails-reloaded before exiting emacs to avoid kernel panics.
(defun kill-ruby ()
  (interactive)
  (shell-command "ps aux | grep [r]ails-reloaded | awk '{print $2}' | xargs kill"))

(defadvice isearch-search (after isearch-no-fail activate)
  (unless isearch-success
    (ad-disable-advice 'isearch-search 'after 'isearch-no-fail)
    (ad-activate 'isearch-search)
    (isearch-repeat (if isearch-forward 'forward))
    (ad-enable-advice 'isearch-search 'after 'isearch-no-fail)
    (ad-activate 'isearch-search)))

(defun directory-p (file)
  (car (file-attributes file)))

(defun acceptable-dir (dir)
  (and (directory-p dir) (not (or (string= "." dir) (string= ".." dir)))))

(defun find-all-the-files (directory)
  (let ((files (directory-files directory)))
    (dolist (file files)
      (if (directory-p file)
          (if (acceptable-dir file) (find-all-the-files file))
        (print file)))))

(defun move-region (start end n)
  "Move the current region up or down by N lines."
  (interactive "r\np")
  (let ((line-text (delete-and-extract-region start end)))
    (forward-line n)
    (let ((start (point)))
      (insert line-text)
      (setq deactivate-mark nil)
      (set-mark start))))

(defun move-region-up (start end n)
  "Move the current line up by N lines."
  (interactive "r\np")
  (move-region start end (if (null n) -1 (- n))))

(defun move-region-down (start end n)
  "Move the current line down by N lines."
  (interactive "r\np")
  (move-region start end (if (null n) 1 n)))

(defun open-line-below ()
  (interactive)
  (end-of-line)
  (newline)
  (indent-for-tab-command))

(defun open-line-above ()
  (interactive)
  (beginning-of-line)
  (newline)
  (forward-line -1)
  (indent-for-tab-command))

(defun goto-match-paren (arg)
  "Go to the matching parenthesis if on parenthesis, otherwise insert %.
vi style of % jumping to matching brace."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
        ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
        (t (self-insert-command (or arg 1)))))

(defun now ()
  (interactive)
  (insert (format-time-string "%H:%M:%S.%3N")))

(defun toggle-quotes ()
  "Toggle single quoted string to double or vice versa, and
  flip the internal quotes as well.  Best to run on the first
  character of the string."
  (interactive)
  (save-excursion
    (re-search-backward "[\"']")
    (let* ((start (point))
           (old-c (char-after start))
           new-c)
      (setq new-c
            (case old-c
              (?\" "'")
              (?\' "\"")))
      (setq old-c (char-to-string old-c))
      (delete-char 1)
      (insert new-c)
      (re-search-forward old-c)
      (backward-char 1)
      (let ((end (point)))
        (delete-char 1)
        (insert new-c)
        (replace-string new-c old-c nil (1+ start) end)))))

(defun yank-buffer-name ()
  (interactive)
  (kill-new (buffer-name (current-buffer)) nil))

(defun my-mark-current-word (&optional arg allow-extend)
  "Put point at beginning of current word, set mark at end."
  (interactive "p\np")
  (setq arg (if arg arg 1))
  (if (and allow-extend
           (or (and (eq last-command this-command) (mark t))
               (region-active-p)))
      (set-mark
       (save-excursion
         (when (< (mark) (point))
           (setq arg (- arg)))
         (goto-char (mark))
         (forward-word arg)
         (point)))
    (let ((wbounds (bounds-of-thing-at-point 'word)))
      (unless (consp wbounds)
        (error "No word at point"))
      (if (>= arg 0)
          (goto-char (car wbounds))
        (goto-char (cdr wbounds)))
      (push-mark (save-excursion
                   (forward-word arg)
                   (point)))
      (activate-mark))))

(defun narrow-to-region-indirect (start end)
  "Restrict editing in this buffer to the current region, indirectly."
  (interactive "r")
  (deactivate-mark)
  (let ((buf (clone-indirect-buffer nil nil)))
    (with-current-buffer buf
      (narrow-to-region start end))
    (switch-to-buffer buf)))

(defun copy-line ()
  "Copy the current line in the kill ring"
  (interactive)
  (kill-ring-save (line-beginning-position) (line-end-position)))

(defun join-region (beg end)
  "Apply join-line over region."
  (interactive "r")
  (if mark-active
      (let ((beg (region-beginning))
            (end (copy-marker (region-end))))
        (goto-char beg)
        (while (< (point) end)
          (join-line 1)))))

(defun serve-folder (folder)
  "Serve the content of FOLDER on port 8010 using elnode."
  (interactive
   (list
    (read-file-name "Serve directory:")))
  (require 'elnode)
  (message "Serving directory %s..." folder)
  (let ((webserver-folder (elnode-webserver-handler-maker folder)))
    (elnode-start webserver-folder :port 8010)))

(defun base64-url-encode (beg end)
  (interactive "r")
  (require 'url-util)
  (if mark-active
      (let ((sel (buffer-substring-no-properties (region-beginning) (region-end))))
        (base64-encode-string (url-encode-url sel)))))

(defun duplicate-line ()
  (interactive)
  (save-excursion
    (copy-line)
    (move-end-of-line nil)
    (newline)
    (yank))
  (next-line))

(defun recompile-init ()
  (interactive)
  (byte-recompile-directory "~/emacs" 0))

;; Cf. http://emacsredux.com/blog/2013/06/21/eval-and-replace/
(defun eval-and-replace ()
  "Replace the preceding sexp with its value."
  (interactive)
  (backward-kill-sexp)
  (condition-case nil
      (prin1 (eval (read (current-kill 0)))
             (current-buffer))
    (error (message "Invalid expression")
           (insert (current-kill 0)))))


(defvar favourite-unicode-characters "☣ ☢ ☠ ☾ ☽ → ← ☞ ☛ ☀ ★ ☉ ☂ ☃ ☤ ☯ ☹ ☺ ♡ ♩ ♪ ♫ ♬ ⚓ ☘")

(defun git-grep (command-args)
  "Use the `grep' machinery to run `git grep'."
  (interactive
   (let ((sap (thing-at-point 'symbol))
         (grep-command "git grep -n "))
     (list (read-shell-command "Run git grep (like this): "
                               (if sap (concat grep-command sap)
                                 grep-command)))))
  (compilation-start (concat "< /dev/null " command-args) 'grep-mode))

(defun magit-toggle-scroll-to-top () (recenter-top-bottom 0))
(advice-add 'magit-toggle-section :after #'magit-toggle-scroll-to-top)

;; (defun clear-string-rectangle-history (start end string) (setq string-rectangle-history nil))
;; (advice-add 'string-rectangle :before #'clear-string-rectangle-history)

(defun magit-log-all ()
  (interactive)
  (magit-key-mode-popup-logging)
  (magit-key-mode-toggle-option (quote logging) "--all"))


(defun replace-in-file (file-to-process regexp value)
  (let ((file-name (expand-file-name file-to-process)))
    (with-temp-file file-name
      (if (file-exists-p file-name)
          (insert-file-contents file-name))
      (set-buffer-file-coding-system 'utf-8)
      (goto-char 1)
      (while (re-search-forward regexp nil t)
        (replace-match value nil nil nil 1)))))

(prefer-coding-system 'utf-8)


;; from http://www.howardism.org/Technical/Emacs/eshell-fun.html
(defun eshell-here ()
  "Opens up a new shell in the directory associated with the
current buffer's file. The eshell is renamed to match that
directory to make multiple eshell windows easier."
  (interactive)
  (let* ((parent (if (buffer-file-name)
                     (file-name-directory (buffer-file-name))
                   default-directory))
         (height (/ (window-total-height) 3))
         (name   (car (last (split-string parent "/" t)))))
    (split-window-vertically (- height))
    (other-window 1)
    (eshell "new")
    (rename-buffer (concat "*eshell: " name "*"))

    (insert (concat "ls"))
    (eshell-send-input)))

(defun eshell/x ()
  (insert "exit")
  (eshell-send-input)
  (delete-window))


;; cf. https://www.reddit.com/r/emacs/comments/jix6od/weekly_tipstricketc_thread/gaxzpm2
(defun eshell/rgr (&rest args)
  "Show rg results for project root in grep-mode."
  (eshell-grep "rg"
		       (append '("--no-heading" "-n" "-H" "-e")
			           args
			           (expand-file-name
			            (project-root (project-current))))))

(defun fix-smoke-tests ()
  (interactive)
  (let ((case-fold-search t)) ; or nil

    (goto-char (point-min))
    (while (search-forward "app_search_input" nil t)
      (replace-match "search-box-input"))

    (goto-char (point-min))
    (while (search-forward "search_box_button" nil t)
      (replace-match "icon-search"))

    (goto-char (point-min))
    (while (search-forward ".item-list tr[ng-repeat=\"merchant in listController.merchants\"]" nil t)
      (replace-match ".table-list tr"))

    (goto-char (point-min))
    (while (search-forward ".list_header span.breadcrumb" nil t)
      (replace-match ".breadcrumb span"))

    (goto-char (point-min))
    (while (search-forward ".list_loading" nil t)
      (replace-match ".page-message-box"))

    (goto-char (point-min))
    (while (search-forward " .clear_filters" nil t)
      (replace-match "[ng-click=\"$ctrl.resetList()\"]"))
    ))

;; Cf. https://stackoverflow.com/a/10365481/289466
(defun generate-buffer ()
  (interactive)
  (switch-to-buffer (make-temp-name "scratch")))


(defvar copy-next-command-output--marker nil)
(defun copy-next-command-output ()
  "Prefix command to add the output of the next command to the `kill-ring`."
  (interactive)
  (let ((ml (minibuffer-depth)))
    (cl-labels ((pre ()
                     (add-hook 'post-command-hook #'post)
                     (setq copy-next-command-output--marker
                           (with-current-buffer "*Messages*"
                             (point-max-marker))))
                (post ()
                      (unless (> (minibuffer-depth) ml)
                        (remove-hook 'pre-command-hook #'pre)
                        (remove-hook 'post-command-hook #'post)
                        (when copy-next-command-output--marker
                          (with-current-buffer
                              (marker-buffer copy-next-command-output--marker)
                            (when (< copy-next-command-output--marker
                                     (point-max))
                              (kill-new (buffer-substring
                                         copy-next-command-output--marker
                                         (point-max)))))
                          (setq copy-next-command-output--marker nil)))))
      (add-hook 'pre-command-hook #'pre))))

(defun javap-handler (op &rest args)
  "Handle .class files by putting the output of javap in the buffer."
  (cond
   ((eq op 'get-file-buffer)
    (let ((file (car args)))
      (with-current-buffer (create-file-buffer file)
        (call-process "javap" nil (current-buffer) nil "-verbose"
                      "-classpath" (file-name-directory file)
                      (file-name-sans-extension (file-name-nondirectory file)))
        (setq buffer-file-name file)
        (setq buffer-read-only t)
        (set-buffer-modified-p nil)
        (goto-char (point-min))
        (java-mode)
        (current-buffer))))
   ((javap-handler-real op args))))

(defun javap-handler-real (operation args)
  "Run the real handler without the javap handler installed."
  (let ((inhibit-file-name-handlers
         (cons 'javap-handler
               (and (eq inhibit-file-name-operation operation)
                    inhibit-file-name-handlers)))
        (inhibit-file-name-operation operation))
    (apply operation args)))


(defun list-extra-dependencies ()
  (interactive)
  (let ((wl-deps nil)
        (v1-deps nil))
    (with-temp-buffer
      (insert-file-contents "/tmp/wl.war.deps")
      (setq wl-deps (split-string (buffer-string) "\n" t)))
    (with-temp-buffer
      (insert-file-contents "/tmp/v1.war.deps")
      (setq v1-deps (split-string (buffer-string) "\n" t)))
    () (-difference wl-deps v1-deps)))


(defun insert-maven-dependency (coord)
  "Transform Gradle dependency coordinates COORD into a Maven dependency block."
  (interactive "sCoordinates: ")
  (let ((parts (split-string coord ":")))
    (insert (format "  <dependency>
    <groupId>%s</groupId>
    <artifactId>%s</artifactId>
    <version>%s</version>
  </dependency>"
                    (nth 0 parts)
                    (nth 1 parts)
                    (nth 2 parts)))))


(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	         (next-win-buffer (window-buffer (next-window)))
	         (this-win-edges (window-edges (selected-window)))
	         (next-win-edges (window-edges (next-window)))
	         (this-win-2nd (not (and (<= (car this-win-edges)
					                     (car next-win-edges))
				                     (<= (cadr this-win-edges)
					                     (cadr next-win-edges)))))
	         (splitter
	          (if (= (car this-win-edges)
		             (car (window-edges (next-window))))
		          'split-window-horizontally
		        'split-window-vertically)))
	    (delete-other-windows)
	    (let ((first-win (selected-window)))
	      (funcall splitter)
	      (if this-win-2nd (other-window 1))
	      (set-window-buffer (selected-window) this-win-buffer)
	      (set-window-buffer (next-window) next-win-buffer)
	      (select-window first-win)
	      (if this-win-2nd (other-window 1))))))

(defun sm-greek-lambda ()
  (font-lock-add-keywords nil
                          `(("\\<lambda\\>"
                             (0 (progn (compose-region (match-beginning 0) (match-end 0)
                                                       ,(make-char 'greek-iso8859-7 107))
                                       nil))))))


(defun take-notes ()
  (interactive)
  (maximize-frame-vertically)
  (let ((current-frame (selected-frame)))
    (modify-frame-parameters
     current-frame
     `((width . 92)
       (left . 1)))))


(defun covid-report ()
  "Insert the current daily report for covid-19; it requires the `kovid` ruby gem to be installed."
  (interactive)
  (org-insert-time-stamp (org-current-time 0))
  (newline 2)
  (insert (shell-command-to-string "kovid check france ireland uk usa germany italy china japan sweden")))

(require 'find-lisp)

(defun delete-stuff (path)
  (interactive)
  (defun my-process-file (fpath)
    (let ((mybuffer (find-file fpath)))
      (goto-char (point-min)) ;; in case buffer is open
      (delete-matching-lines "transfervergleich")

      (save-buffer)
      (kill-buffer mybuffer)))
  (mapc 'my-process-file (find-lisp-find-files path "\\.properties$")))


;; crypto functions

(defun sha256 (object)
  "Return SHA256 cryptographic hash of OBJECT."
  (secure-hash 'sha256 object))

(defun sha512 (object)
  "Return SHA512 cryptographic hash of OBJECT."
  (secure-hash 'sha512 object))


;; Cf. https://emacsredux.com/blog/2020/09/12/reinstalling-emacs-packages/
(defun er-reinstall-package (pkg)
  (interactive (list (intern (completing-read "Reinstall package: " (mapcar #'car package-alist)))))
  (unload-feature pkg)
  (package-reinstall pkg)
  (require pkg))


;; Cf. https://emacs.stackexchange.com/a/29389/724
(defun org-toc ()
  "Generate a table of contents for org-files in this directory."
  (interactive)
  (let ((org-agenda-files (f-entries "." (lambda (f) (f-ext? f "org")) t)))
    (helm-org-agenda-files-headings)))


(defun org-toc-create-index ()
  (interactive)
  (let ((headings (delq nil (cl-loop for f in (f-entries "." (lambda (f) (f-ext? f "org")) t)
                                     append
                                     (with-current-buffer (find-file-noselect f)
                                       (org-map-entries
                                        (lambda ()
                                          (when (> 2 (car (org-heading-components)))
                                            (cons f (nth 4 (org-heading-components)))))))))))
    (switch-to-buffer (get-buffer-create "*toc*"))
    (erase-buffer)
    (org-mode)
    (cl-loop for (file . heading) in headings
             do
             (insert (format "* [[%s::*%s]]\n" file heading)))))

;; Org-roam functions

;; Shamelessly nicked from
;; https://github.com/zaeph/.emacs.d/blob/4548c34d1965f4732d5df1f56134dc36b58f6577/init.el#L2822-L2875

(defun zp/org-find-time-file-property (property &optional anywhere)
  "Return the position of the time file PROPERTY if it exists.
When ANYWHERE is non-nil, search beyond the preamble."
  (save-excursion
    (goto-char (point-min))
    (let ((first-heading
           (save-excursion
             (re-search-forward org-outline-regexp-bol nil t))))
      (when (re-search-forward (format "^#\\+%s:" property)
                               (if anywhere nil first-heading)
                               t)
        (point)))))

(defun zp/org-has-time-file-property-p (property &optional anywhere)
  "Return the position of time file PROPERTY if it is defined.
As a special case, return -1 if the time file PROPERTY exists but
is not defined."
  (when-let ((pos (zp/org-find-time-file-property property anywhere)))
    (save-excursion
      (goto-char pos)
      (if (and (looking-at-p " ")
               (progn (forward-char)
                      (org-at-timestamp-p 'lax)))
          pos
        -1))))

(defun zp/org-set-time-file-property (property &optional anywhere pos)
  "Set the time file PROPERTY in the preamble.
When ANYWHERE is non-nil, search beyond the preamble.
If the position of the file PROPERTY has already been computed,
it can be passed in POS."
  (when-let ((pos (or pos
                      (zp/org-find-time-file-property property))))
    (save-excursion
      (goto-char pos)
      (if (looking-at-p " ")
          (forward-char)
        (insert " "))
      (delete-region (point) (line-end-position))
      (let* ((now (format-time-string "[%Y-%m-%d %a %H:%M]")))
        (insert now)))))

(defun zp/org-set-last-modified ()
  "Update the LAST_MODIFIED file property in the preamble."
  (when (derived-mode-p 'org-mode)
    (zp/org-set-time-file-property "LAST_MODIFIED")))


(defun elfeed-goodies/search-header-draw ()
  "Returns the string to be used as the Elfeed header."
  (if (zerop (elfeed-db-last-update))
      (elfeed-search--intro-header)
    (let* ((separator-left (intern (format "powerline-%s-%s"
                                           elfeed-goodies/powerline-default-separator
                                           (car powerline-default-separator-dir))))
           (separator-right (intern (format "powerline-%s-%s"
                                            elfeed-goodies/powerline-default-separator
                                            (cdr powerline-default-separator-dir))))
           (db-time (seconds-to-time (elfeed-db-last-update)))
           (stats (-elfeed/feed-stats))
           (search-filter (cond
                           (elfeed-search-filter-active
                            "")
                           (elfeed-search-filter
                            elfeed-search-filter)
                           (""))))
      (if (>= (window-width) (* (frame-width) elfeed-goodies/wide-threshold))
          (search-header/draw-wide separator-left separator-right search-filter stats db-time)
        (search-header/draw-tight separator-left separator-right search-filter stats db-time)))))

(defun elfeed-goodies/entry-line-draw (entry)
  "Print ENTRY to the buffer."

  (let* ((title (or (elfeed-meta entry :title) (elfeed-entry-title entry) ""))
         (date (elfeed-search-format-date (elfeed-entry-date entry)))
         (title-faces (elfeed-search--faces (elfeed-entry-tags entry)))
         (feed (elfeed-entry-feed entry))
         (feed-title
          (when feed
            (or (elfeed-meta feed :title) (elfeed-feed-title feed))))
         (tags (mapcar #'symbol-name (elfeed-entry-tags entry)))
         (tags-str (concat "[" (mapconcat 'identity tags ",") "]"))
         (title-width (- (window-width) elfeed-goodies/feed-source-column-width
                         elfeed-goodies/tag-column-width 4))
         (title-column (elfeed-format-column
                        title (elfeed-clamp
                               elfeed-search-title-min-width
                               title-width
                               title-width)
                        :left))
         (tag-column (elfeed-format-column
                      tags-str (elfeed-clamp (length tags-str)
                                             elfeed-goodies/tag-column-width
                                             elfeed-goodies/tag-column-width)
                      :left))
         (feed-column (elfeed-format-column
                       feed-title (elfeed-clamp elfeed-goodies/feed-source-column-width
                                                elfeed-goodies/feed-source-column-width
                                                elfeed-goodies/feed-source-column-width)
                       :left)))

    (if (>= (window-width) (* (frame-width) elfeed-goodies/wide-threshold))
        (progn
          (insert (propertize date 'face 'elfeed-search-date-face) " ")
          (insert (propertize feed-column 'face 'elfeed-search-feed-face) " ")
          (insert (propertize tag-column 'face 'elfeed-search-tag-face) " ")
          (insert (propertize title 'face title-faces 'kbd-help title)))
      (insert (propertize title 'face title-faces 'kbd-help title)))))


(defun delete-invalid-ref (file)
  (with-temp-file file
    (insert-file-contents file)
    (delete-matching-lines "^:ROAM_REFS:[[:space:]]+[^:]+$")))


(defun delete-invalid-ref-all-roam-files ()
  (let ((files (directory-files org-roam-directory t ".*\.org")))
    (mapcar #'delete-invalid-ref files)))


(cl-defstruct org-document title path)

(defun create-org-doc-entry (file)
  (let ((title nil))
    (with-temp-buffer
      (insert-file-contents file)
      (pcase (org-collect-keywords '("TITLE"))
        (`(("TITLE" . ,val))
         (setq title (car val)))))
    (make-org-document :title title :path file)))

(defun create-index-file-org-roam ()
  (let ((files (directory-files org-roam-directory t ".*\.org")))
    (mapcar #'create-org-doc-entry files)))

(defun smarter-tab-to-complete ()
  "Try to `org-cycle', `yas-expand', and `yas-next-field' at current cursor position.

If all failed, try to complete the common part with `company-complete-common'"
  (interactive)
  (when yas-minor-mode
    (let ((old-point (point))
          (old-tick (buffer-chars-modified-tick))
          (func-list
           (if (equal major-mode 'org-mode) '(org-cycle yas-expand yas-next-field)
             '(yas-expand yas-next-field))))
      (catch 'func-suceed
        (dolist (func func-list)
          (ignore-errors (call-interactively func))
          (unless (and (eq old-point (point))
                       (eq old-tick (buffer-chars-modified-tick)))
            (throw 'func-suceed t)))
        (company-complete-common)))))

(defun tycho/kill-this-buffer ()
  (interactive)
  (if (minibufferp (current-buffer))
      (abort-recursive-edit)
    (kill-buffer (current-buffer))))

(defun tycho/kill-helm-buffers ()
  (interactive)
  (save-excursion
    (dolist (buf (buffer-list (current-buffer)))
      (with-current-buffer buf
        (when (and (stringp mode-name)
                   (string-match "Hmm" mode-name))
          (kill-buffer buf))))))

;; Open gedit from emacs, because sometimes you feel like
;; you need to... 😆
(defun tycho/open-in-gedit ()
  (interactive)
  (async-shell-command (format "gedit %s" buffer-file-name)))

;; From https://www.reddit.com/r/emacs/comments/pt2xws/comment/heaxg00/
(cl-defmacro ph/mapconcat (&key (applying #'identity) across (separating-with " "))
  "Apply APPLYING across ACROSS separating with SEPARATING-WITH.
APPLYING defaults to `identity'. SEPARATING-WITH defaults to a single space."
  `(mapconcat (function ,applying) ,across ,separating-with))

;; (ert-deftest ph/mapconcat ()
;;   "Test ph/mapconcat."
;;   :tags '(strings)
;;   (should
;;    (string=
;;     (ph/mapconcat :across '("subdomain" "domain" "tld") :separating-with ".")
;;     "subdomain.domain.tld"))
;;   (should
;;    (string=
;;     (ph/mapconcat :across '("Hello" "world!"))
;;     "Hello world!")))


(defun uuid-insert ()
  (interactive)
  (shell-command "uuidgen" t))

;; from https://jethrokuan.github.io/org-roam-guide/
(defun jethro/org-roam-node-from-cite (keys-entries)
  (interactive (list (citar-select-ref :multiple nil :rebuild-cache t)))
  (let ((title (citar--format-entry-no-widths (cdr keys-entries)
                                              "${author editor} :: ${title}")))
    (org-roam-capture- :templates
                       '(("r" "Reference" plain "%?"
                          :if-new (file+head "~/org/org-roam/reference/${citekey}.org"
                                     ":PROPERTIES:
:ROAM_REFS: [cite:@${citekey}]
:END:
#+title: ${title}
#+created: %u
#+last_modified: %U\n\n")
                          :immediate-finish t
                          :unnarrowed t))
                       :info (list :citekey (car keys-entries))
                       :node (org-roam-node-create :title title)
                       :props '(:finalize find-file))))



;; from https://gist.github.com/larrasket/4188cac7d0d034eda6ea99160e94dc57
(defun org-babel-execute:chess (body params)
  "Execute a block of Chess code with org-babel.
This function is called by `org-babel-execute-src-block'."
  (let* ((output-file (concat (file-name-sans-extension (buffer-file-name)) (format "_%s_chess_output.svg" (format-time-string "%Y-%m-%d_%H-%M-%S")) ))
         (notation (cdr (assq :notation params)))
         (extension (if (equal notation "fen") ".fen" ".pgn"))
         (notation-file (make-temp-file "chess-notation" nil extension))
         (cmd (format "python ~/bin/elchess.py %s %s %s" notation-file output-file notation)))
    (with-temp-buffer
      (insert body)
      (write-file notation-file))
    (shell-command cmd)
    (org-babel-result-to-file (file-name-nondirectory output-file))))


(setq org-babel-default-header-args:chess
      '((:results . "raw")))

;; end of file
